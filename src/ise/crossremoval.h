#ifndef CROSSREMOVAL_H
#define CROSSREMOVAL_H

#include <QMainWindow>
#include "ui_crossremoval.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkDerivativeImageFilter.h"
#include "itkSmoothingRecursiveGaussianImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkMedianImageFilter.h"
#include <QStringList>

class YK16GrayImage;

#define BLACK_VALUE 0
#define NORMAL_VALUE 1000
#define WHITE_VALUE 4095
#define CENTERLINE_VALUE 30000

#define DEFAULT_CROSSHAIR_MARGIN 20
#define DEFAULT_SAMPLING_PIXELS 10
#define DEFAULT_ROI_RATIO_X 0.5
#define DEFAULT_ROI_RATIO_Y 0.5

#define INCL_DIFF_THRESHOLD 0.3
#define DEFAULT_PIX_DIFF_PERCENT 30 //30%

#define MASK_CROSSHAIR 2
#define MASK_NONCROSSHAIR 0

struct IMAGEPROC_PARAM
{
    int MEDIAN_size;
    int GAUSSIAN_sigma;
    int	additionalMargin;
    double ROI_RatioX;
    double ROI_RatioY;
    double continuityThreshold;
};

struct CROSSHAIRINFO
{
    double GradientHor;
    double yCutHor;
    int thickenssHor;
    double GradientVer;
    double yCutVer;
    int thickenssVer;
	
    YK16GrayImage* pMaskImg;
};

typedef itk::Image<float,2> FloatImageType;
typedef itk::Image<unsigned short, 2> UnsignedShortImageType;
typedef itk::ImageFileReader<UnsignedShortImageType> readerType;
typedef itk::DerivativeImageFilter<UnsignedShortImageType, FloatImageType> DerivativeFilterType;
typedef itk::SmoothingRecursiveGaussianImageFilter<UnsignedShortImageType, UnsignedShortImageType>  SmoothingFilterType;
typedef itk::IntensityWindowingImageFilter<UnsignedShortImageType, UnsignedShortImageType>  WindowingFilterType;
typedef itk::MedianImageFilter<UnsignedShortImageType,UnsignedShortImageType> MedianFilterType;


class CrossRemoval : public QMainWindow
{
    Q_OBJECT

public:
    CrossRemoval(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    ~CrossRemoval();

    enum DIRECTION{
        HORIZONTAL = 0,
        VERTICAL
    };

    enum REPLACEOPTION{
        XY_FROM_MEDIAN = 0, //general use
        Y_FROM_ORIGINAL //less cross-hair artifact: Can be used for oblique cross-hair
    };
    bool MedianFiltering(YK16GrayImage* pImage, int medWindow);
    bool GaussianFiltering(YK16GrayImage* pImage, double sigma);
    //bool DerivativeFiltering(YK16GrayImage* pImage, int direction);
    //bool ReplacingCrosshairRegion (YK16GrayImage* pImage, double sigma, int direction);
    bool DerivativeFiltering( YK16GrayImage* pImage, int direction );
    //void GetLineEq(int direction, YK16GrayImage* pDerivativeImage, double* Grad, double* yCut, int* thickness, double ROI_RatioX, double ROI_RatioY); // 0:hor, 1: vert
    void GetLineEqFromDerivativeImg(int direction, YK16GrayImage* pDerivativeImage, double* Grad, double* yCut, int* thickness, double ROI_RatioX, double ROI_RatioY, double inclThreshold); // 0:hor, 1: vert
    void GenerateMaskImgForSingle(int direction, YK16GrayImage* pTargetMaskImg, int margin, double Grad, double yCut);//margin: half margin

    //void GetCrosshairMask(IMAGEPROC_PARAM& imgProcParam, CROSSHAIRINFO* crossInfo, YK16GrayImage* srcImg, YK16GrayImage* pTargetReplacedImg,  YK16GrayImage* pTargetMaskImg);
    void GetCrosshairMask(IMAGEPROC_PARAM& imgProcParam, CROSSHAIRINFO* crossInfo, YK16GrayImage* srcImg, YK16GrayImage* pTargetReplacedImg, YK16GrayImage* pTargetMaskImg, int ReplacementOption);
	
    //arrImg is supposed to be normalized first. but for the test purpose it can be also used w/o normalization
    //Median value should be selected among valid pixel only. In Mask, MASK_CROSSHAIR pixel will not be excluded in median calculation
    //void GeneratePixMedianImg (YK16GrayImage* pTargetImage, int arrSize, YK16GrayImage* arrImg, YK16GrayImage* arrImgMask);
    void GeneratePixMedianImg( YK16GrayImage* pTargetImage, int arrSize, YK16GrayImage* arrImg, YK16GrayImage* arrImgMask, YK16GrayImage* arrImgReplaced, int refIdx );
    //unsigned short GetMedianPixValueFromMultipleImg(int pxIdx, int arrSize, YK16GrayImage* arrImg, YK16GrayImage* arrImgMask, int* sampleCnt );
    unsigned short GetMedianPixValueFromMultipleImg( int pxIdx, int arrSize, YK16GrayImage* arrImg, YK16GrayImage* arrImgMask, YK16GrayImage* arrReplacedImg,int iRefIdx, int* sampleCnt );

    void Gen2x2BinImg( YK16GrayImage* pImage14, YK16GrayImage* pImage23, YK16GrayImage* pTarImg );
    void ReleaseMemory();

    //pSrc1: replacedImage (not original image)
    //void VertLineBasedReplacement(YK16GrayImage* pSrc1,YK16GrayImage* pMask1, YK16GrayImage* pSrc2,YK16GrayImage* pMask2, YK16GrayImage* pTarImg);
    //void PerLineReplacement(int rowSize, unsigned short* VertLineSrc, unsigned short* VertLineSrcMask, unsigned short* VertLineRef, unsigned short* VertLineRefMask); //only VertLineSrc will be changed

public slots:
    void SLT_LoadImage();//Load multipleimage

    void SLT_DrawCurImage();
    void SLT_SaveAs();
    void SLT_About();
    void SLT_SetCurImageAsNewSourceImage();

    void SLT_CrosshairDetection();
    void SLT_SetRefImage();

    void SLT_NormCalc();
    void SLT_Normalization();
    void SLT_PixMedianImg();
    void SLT_SaveMultipleMask();
	
public:
    unsigned short GetSubstituteVert(int x,int y, double coeff0, double coeff1, int halfThickness, int iSamplePixels, unsigned short* medianImg, int width, int height);
    unsigned short GetSubstituteHor(int x,int fixedY, double coeff0, double coeff1, int halfThickness, int iSamplePixels, unsigned short* medianImg, int width, int height);

    bool SaveAutoFileName(QString& srcFilePath, QString endFix); //YKCur image default
    bool SaveAutoFileName(YK16GrayImage* pYK16Img, QString& srcFilePath, QString endFix);


public:
    YK16GrayImage* m_pImageYKSrc;
    YK16GrayImage* m_pImageYKCur; //	
    YK16GrayImage* m_pMedianYKImg;

    int m_iFileCnt;

    QString m_strSrcFilePath;

    UnsignedShortImageType::Pointer m_itkCurImage;
    int m_iWidth;
    int m_iHeight;

    int m_iRefImageIdx;
    int m_iCrosshairMargin;
    double m_fSearchROI_X;
    double m_fSearchROI_Y;

    YK16GrayImage* m_pMaskComposite;
    YK16GrayImage* m_arrYKImage;          //image array
    YK16GrayImage* m_arrYKImageMask;      //image array	
    YK16GrayImage* m_arrYKImageReplaced;  //image array	
    CROSSHAIRINFO* m_pCrosshairInfo;
    QStringList m_strListSrcFilePath;


    YK16GrayImage* m_pPixelMedianImage;
    // multiple image 에서 median 값으로 구성한 composite image. it will display statistic info. about median sample number.
    //this image should be prepared after normalization
    //this image can potentially be used for gain correction


private:
    Ui::CrossRemovalClass ui;
};

#endif // CROSSREMOVAL_H
