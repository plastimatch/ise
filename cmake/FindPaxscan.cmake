# Varian Paxscan
# These variables are set:
#
#   PAXSCAN_FOUND
#   PAXSCAN_INCLUDE_DIR
#   PAXSCAN_LIBRARIES
#
# Only works on Windows

if (PAXSCAN_INCLUDE_DIR)
  # Already in cache, be silent
  set (Paxscan_FIND_QUIETLY TRUE)
endif ()

find_path (PAXSCAN_L04_INCLUDE_DIR 
  NAMES "HcpFuncDefs.h" "vip_comm.h"
  PATHS
  "C:/Program Files/Varian/PaxscanL04/DeveloperFiles/Includes"
  "C:/Program Files/Varian/Paxscan/vip_comm"
  )

if (PAXSCAN_L04_INCLUDE_DIR)
  set (PAXSCAN_INCLUDE_DIR "${PAXSCAN_L07_INCLUDE_DIR}")
  find_library (PAXSCAN_LIBRARIES 
    NAMES "VirtCp.lib" "vip_comm.lib"
    PATHS
    "C:/Program Files/Varian/PaxscanL04/DeveloperFiles/VirtCpRel"
    "C:/Program Files/Varian/Paxscan/vip_comm"
    )
else ()
  find_path (PAXSCAN_L07_INCLUDE_DIR 
    NAME "HcpFuncDefs.h"
    PATHS
    "C:/Program Files/Varian/PaxscanL07/developer/Includes"
    )
  if (PAXSCAN_L07_INCLUDE_DIR)
    set (PAXSCAN_INCLUDE_DIR "${PAXSCAN_L07_INCLUDE_DIR}")
  endif ()
  find_library (PAXSCAN_LIBRARIES 
    NAMES "VirtCp64.lib"
    PATHS
    "C:/Program Files/Varian/PaxscanL07/developer/VirtCpRel64"
    )
endif ()

include (FindPackageHandleStandardArgs)
find_package_handle_standard_args (Paxscan DEFAULT_MSG 
  PAXSCAN_LIBRARIES PAXSCAN_INCLUDE_DIR)
