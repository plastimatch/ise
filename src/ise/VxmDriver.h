#ifndef __VD_H__		//If not defined
	
#define __VD_H__		//Define	

#include "Windows.h"

//--- Declare the wrapper functions ---//
long LoadDriver(LPCSTR DriverPath);
long ReleaseDriver();
long DriverTerminalShowState(long StateToShow, long ParentHwnd);
long PortOpen(long PortNumber, long BaudRate);
long PortIsOpen(void);
long PortClose(void);
long PortSendCommands(char* CommandToSend);
char* PortReadReply(void);
long PortCountChars(void);
long PortSearchForChars(char* CharsToFind);
long PortClear(void);
long PortRemoveChars(char* StringToRemove);
char* MotorPosition(long MotorNumber);
long PortWaitForChar(char* CharToWaitFor, long TimeOutTime);
long PortWaitForCharWithMotorPosition(char* CharToWaitFor, long MotorNumber, long ReportToWindowHwnd, long TimeOutTime);
void DriverResetFunctions(void);

#endif                      //End macro