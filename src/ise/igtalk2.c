/* -------------------------------------------------------------------------*
    See COPYRIGHT for copyright information.
 * -------------------------------------------------------------------------*/
#define _USE_32BIT_TIME_T 1
#include "config.h"
#include "ise.h"
#include "ise_config.h"
#include "ise_framework.h"
#include "ise_globals.h"
#include "debug.h"
#include "panel.h"
#include "indico_info.h"

/* ---------------------------------------------------------------------------- *
    Global variables
 * ---------------------------------------------------------------------------- */
typedef struct dips_info_struct Dips_Info;
struct dips_info_struct {
    HANDLE panelh;
    HANDLE pixelh;
    struct PANEL* panelp;
    unsigned short* pixelp;
};
Dips_Info dips_info[2];

Indico_Info indico_info;

unsigned long mode;
char *client_ip_1, *client_ip_2;
char *server_ip_1, *server_ip_2;
int board_1, board_2;
int flip_1, flip_2;
double framerate_1, framerate_2;

typedef struct igtalk2_struct Igtalk2;
struct igtalk2_struct {
    int allow_frame;
    int prev_bright;
    int display_id;
    int display_ts;
    int acc_frames;
    int waiting_time;
    int sent_to_dips;
};
Igtalk2 igtalk2[2];

/* ---------------------------------------------------------------------------- *
    Global functions
 * ---------------------------------------------------------------------------- */
void
parse_command_line (int argc, char* argv[])
{
    if (argc != 7 && argc != 12) {
	printf ("igtalk2 version %s\n", ISE_VERSION);
        printf ("Usage: igtalk2 mode client_ip server_ip board flip framerate [cip sip bd fl fr]\n");
	exit (2);
    }

    if (!strcmp (argv[1], "matrox") || !strcmp (argv[1], "mil")) {
	mode = ISE_IMAGE_SOURCE_MATROX_HIRES_FLUORO;
#if !HAVE_MIL
	printf("Error: igtalk2 was not compiled with matrox support.\n");
	exit(2);
#endif
    } else if (!strcmp (argv[1], "bitflow")) {
	mode = ISE_IMAGE_SOURCE_BITFLOW_LORES_FLUORO;
#if !HAVE_BITFLOW
	printf("Error: igtalk2 was not compiled with bitflow support.\n");
	exit(2);
#endif
    } else {
	printf("mode must be either \"matrox\" or \"bitflow\".\n");
	exit(2);
    }

    client_ip_1 = argv[2];
    server_ip_1 = argv[3];
    board_1 = atoi(argv[4]);
    flip_1 = atoi(argv[5]);
    framerate_1 = atof(argv[6]);
    if (framerate_1 > 7.5f || framerate_1 < 1.0f) {
	printf("Framerate must be between 1.0 and 7.5\n");
	exit(2);
    }
    if (argc == 11) {
	client_ip_2 = argv[7];
	server_ip_2 = argv[8];
	board_2 = atoi(argv[9]);
	flip_2 = atoi(argv[10]);
	framerate_2 = atof(argv[11]);
	if (framerate_2 > 7.5f || framerate_2 < 1.0f) {
	    printf("Framerate must be between 1.0 and 7.5\n");
	    exit(2);
	}
    } else {
	client_ip_2 = 0;
	server_ip_2 = 0;
	board_2 = -1;
	flip_2 = 0;
	framerate_2 = 0;
    }
}

void
init_dips_interface_1 (int selector)
{
    int cmd_rc = 0;
    char panel_name[12], pixel_name[12];
    Dips_Info* di = &dips_info[selector];

    /* Set up shared memory */
    sprintf (panel_name, "PANEL%i", selector);
    sprintf (pixel_name, "PIXEL%i", selector);
    di->panelh = CreateFileMapping (INVALID_HANDLE_VALUE, NULL, 
					PAGE_READWRITE, 0, sizeof (struct PANEL), 
					panel_name);
    if (!di->panelh) {
	fprintf (stderr, "Error opening shared memory for panel\n");
	exit (1);
    }
    di->pixelh = CreateFileMapping (INVALID_HANDLE_VALUE, NULL, 
			PAGE_READWRITE,	0, HIRES_IMAGE_WIDTH*HIRES_IMAGE_HEIGHT*2, 
			pixel_name);
    if (!di->panelh) {
	fprintf (stderr, "Error opening shared memory for pixel\n");
	exit (1);
    }
    di->panelp = (struct PANEL*) MapViewOfFile (di->panelh, FILE_MAP_ALL_ACCESS, 0, 0, 0);
    if (!di->panelp) {
	fprintf (stderr, "Error mapping shared memory for panel\n");
	exit (1);
    }
    di->pixelp = (unsigned short*) MapViewOfFile (di->pixelh, 
	FILE_MAP_ALL_ACCESS, 0, 0, 0);
    if (!di->pixelp) {
	fprintf (stderr, "Error mapping shared memory for pixel\n");
	exit (1);
    }
    di->panelp->status = READ;
    di->panelp->time = 0;
    di->panelp->ale = 0;
    di->panelp->xs = HIRES_IMAGE_WIDTH;
    di->panelp->ys = HIRES_IMAGE_HEIGHT;
    di->panelp->depth = 2;
    di->panelp->pixel = (short*) di->pixelp;
}

void
init_dips_interface ()
{
    init_dips_interface_1 (0);
    if (client_ip_2) {
	init_dips_interface_1 (1);
    }
}

__inline unsigned short
combine_pixels_1 (unsigned short p1, unsigned short p2)
{
    const int saturation_value = 9800;

    if (p1 > saturation_value) {
	p1 = saturation_value;
    }
    if (p2 > saturation_value) {
	p2 = saturation_value;
    }
    return p1 + p2;
}

__inline unsigned short
combine_pixels_2 (unsigned short p1, unsigned short p2)
{
    const int saturation_value = 9800;

    if (p2 > saturation_value) {
	p2 = saturation_value;
    }
    return p1 + p2;
}

void
send_frame_to_dips (int idx)
{
//    printf ("\nSending image to dips...\n");
//    debug_printf ("Sending image to dips...\n");

    /* Make sure dips has already read in the previous image... */
    if ((dips_info[idx].panelp->status | READ) == 0) {
	return;
    }

    /* Timestamp the image */
    time (&dips_info[idx].panelp->time);

    /* Let DIPS know we have an image */
    dips_info[idx].panelp->status = VALID;
}

/* Display status:
    0 curr dark, prev dark
    1 curr bright, prev bright
    2 curr dark, prev bright
    3 curr bright, prev bright
*/
void
display_info (Frame* frame[2], double base_timestamp)
{
    int idx = 0;
    int pre_transition = 0, post_transition = 0;
    static int suppress_pre_transition = 0;

#define disp_format "%5d %5d %7s %c %d %2d   %5d %5d %7s %c %d %2d"

    for (idx = 0; idx < globals.ig.num_idx; idx++) {
	Frame* f = frame[idx];
	if (!f) continue;

	igtalk2[idx].display_id = f->id;
	igtalk2[idx].display_ts = (int) (f->timestamp - base_timestamp);
	if (frame_is_dark(f)) {
	    if (igtalk2[idx].prev_bright) {
		pre_transition = 1;
	    }
	    igtalk2[idx].prev_bright = 0;
	} else {
	    if (!igtalk2[idx].prev_bright) {
		pre_transition = 1;
	    }
	    igtalk2[idx].prev_bright = 1;
	}
	if (igtalk2[idx].sent_to_dips) {
	    post_transition = 1;
	}
	if (indico_info.shmem->rad_state[idx] == INDICO_SHMEM_XRAY_ON) {
	    post_transition = 1;
	}
    }
    if (pre_transition && !suppress_pre_transition) {
	printf ("\n");
    }
    printf (disp_format "\r", 
	igtalk2[0].display_id, igtalk2[0].display_ts, 
	igtalk2[0].prev_bright ? "Bright" : " Dark ",
	igtalk2[0].sent_to_dips ? 'D' : ' ',
	indico_info.shmem->rad_state[0], 
	igtalk2[0].allow_frame, 
	igtalk2[1].display_id, igtalk2[1].display_ts, 
	igtalk2[1].prev_bright ? "Bright" : " Dark ",
	igtalk2[1].sent_to_dips ? 'D' : ' ', 
	indico_info.shmem->rad_state[1],
	igtalk2[1].allow_frame
	);
    for (idx = 0; idx < globals.ig.num_idx; idx++) {
	igtalk2[idx].sent_to_dips = 0;
    }
    if (post_transition) {
	suppress_pre_transition = 1;
	printf ("\n");
    } else {
	suppress_pre_transition = 0;
    }
}

/* This accumulates 3 frames */
void
check_frame_for_dips (int idx, Frame* frame)
{
    int i;
    //const int WAIT_TIME = 15;
    const int WAIT_TIME = 0;
    const int ALLOW_FRAME_COUNT = 20;

    if (indico_info.shmem->rad_state[idx] > 0) {
	igtalk2[idx].allow_frame = ALLOW_FRAME_COUNT;
    }

    if (igtalk2[idx].waiting_time > 0) igtalk2[idx].waiting_time--;

    switch (igtalk2[idx].acc_frames) {
    case 3:
	if (!frame_is_dark(frame)) {
	    memcpy (dips_info[idx].pixelp, frame->img, 2*HIRES_IMAGE_WIDTH*HIRES_IMAGE_HEIGHT);
	    igtalk2[idx].acc_frames = 2;
	    igtalk2[idx].waiting_time = WAIT_TIME;
	}
	if (--igtalk2[idx].allow_frame < 0) igtalk2[idx].allow_frame = 0;
	break;
    case 2:
	for (i = 0; i < HIRES_IMAGE_WIDTH*HIRES_IMAGE_HEIGHT; i++) {
	    dips_info[idx].pixelp[i] = combine_pixels_1 (dips_info[idx].pixelp[i], frame->img[i]);
	}
	igtalk2[idx].acc_frames--;
	break;
    case 1:
	for (i = 0; i < HIRES_IMAGE_WIDTH*HIRES_IMAGE_HEIGHT; i++) {
	    dips_info[idx].pixelp[i] = combine_pixels_2 (dips_info[idx].pixelp[i], frame->img[i]);
	}
	if (igtalk2[idx].allow_frame > 0) {
	    igtalk2[idx].sent_to_dips = 1;
	    send_frame_to_dips (idx);
	}
	igtalk2[idx].acc_frames--;
	break;
    case 0:
	if (igtalk2[idx].waiting_time <= 0 && frame_is_dark(frame)) {
	    igtalk2[idx].acc_frames = 3;
	}
	igtalk2[idx].allow_frame = 0;
	break;
    }
}

int
do_main_loop (void)
{
    int idx;
    Frame* frame[2];
    double base_timestamp = 0.0;

    while (1) {
	/* Check for program end */
	// if (globals.quit) break;

	/* BLT fluoro to screen */
	for (idx = 0; idx < globals.ig.num_idx; idx++) {
	    /* GCS FIX - this is done to remove dropped display entry from debug log */
	    globals.notify[idx] = 0;
	    if (frame[idx] = ise_fluoro_get_next (idx)) {
		if (base_timestamp == 0.0) {
		    base_timestamp = frame[idx]->timestamp;
		}
		//debug_printf ("ANALYZE %d %d\n", idx, frame[idx]->id);
		check_frame_for_dips (idx, frame[idx]);
	    }
	}
	display_info (frame, base_timestamp);

        /* Give up timeslice */
	Sleep (50);
    }

    return 0;
}

int 
main (int argc, char* argv[])
{
    int rc;

    init_globals ();
    igtalk2[0].allow_frame = 0;
    igtalk2[1].allow_frame = 0;
    igtalk2[0].acc_frames = 3;
    igtalk2[1].acc_frames = 3;
    igtalk2[0].waiting_time = 0;
    igtalk2[1].waiting_time = 0;
    igtalk2[0].prev_bright = 0;
    igtalk2[1].prev_bright = 0;
    igtalk2[0].display_id = 0;
    igtalk2[1].display_id = 0;
    igtalk2[0].display_ts = 0;
    igtalk2[1].display_ts = 0;
    igtalk2[0].sent_to_dips = 0;
    igtalk2[1].sent_to_dips = 0;

    parse_command_line (argc, argv);

    globals.drop_dark_frames = 0;
    init_dips_interface ();
    init_indico_shmem (&indico_info);

    printf ("**************************************\n");
    printf ("*          IGTALK %5s              *\n", ISE_VERSION);
    printf ("**************************************\n");

    rc = ise_startup (mode, 
	client_ip_2 ? 2 : 1,
	client_ip_1,
	server_ip_1,
	board_1,
	flip_1,
	IGTALK2_NUM_FRAMES,
	framerate_1,
	client_ip_2,
	server_ip_2,
	board_2,
	flip_2,
	IGTALK2_NUM_FRAMES,
	framerate_2
	);

    if (rc) {
	 exit (-1);
    }

    ise_fluoro_start_grabbing ();

    printf ("**************************************\n");
    printf ("*          IGTALK %5s              *\n", ISE_VERSION);
    printf ("**************************************\n");
    printf ("*          OK TO START DIPS          *\n");
    printf ("**************************************\n");

#if defined (commentout)
    MessageBox (NULL, "Rapid frame acquisition", "Rapid frame acquisition",
		MB_OK | MB_SYSTEMMODAL);
#endif

    do_main_loop ();

    ise_shutdown ();
    return 0;
}
