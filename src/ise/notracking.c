/* -------------------------------------------------------------------------*
    See COPYRIGHT for copyright information.
 * -------------------------------------------------------------------------*/
/* -------------------------------------------------------------------------*
   Stub routines, for the igtalk2 application.
 * -------------------------------------------------------------------------*/
#include "ise.h"

void
tracker_init (TrackerInfo* ti)
{
}

void
track_frame (IseFramework* ig, unsigned int idx, Frame* f)
{
}

void
tracker_shutdown (TrackerInfo* ti)
{
}

OntrakData* 
ise_ontrak_init (void)
{
    return 0;
}

void
ise_ontrak_engage_relay (OntrakData* od, int gate_beam, int bright_frame)
{
}

void
ise_ontrak_shutdown (OntrakData* od)
{
}
