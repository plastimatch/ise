/* -------------------------------------------------------------------------*
    See COPYRIGHT for copyright information.
 * -------------------------------------------------------------------------*/
#ifndef __BEAMON_H__
#define __BEAMON_H__

#include "NIDAQmx.h"

#define DAQmxErrChk(functionCall) { if( DAQmxFailed(error_bo=(functionCall)) ) { goto Error; } }

extern HANDLE*     beam_on_thread_handle;
extern int32       error_bo;
extern TaskHandle  taskHandle_bo;
extern char        errBuff_bo[2048];

void beam_on_init (void);
int beam_on_stop (HANDLE* beam_on_thread_handle);


#endif