/* -------------------------------------------------------------------------*
    See COPYRIGHT for copyright information.
 * -------------------------------------------------------------------------*/
#ifndef __indico_h__
#define __indico_h__

#define INDICO_SHMEM_STRING "ISE_INDICO_SHMEM"

typedef struct indico_shmem_struct Indico_Shmem;
struct indico_shmem_struct {
    int rad_state[2];
};

typedef struct indico_info_struct Indico_Info;
struct indico_info_struct {
    HANDLE h_shmem;
    Indico_Shmem* shmem;
};

#endif
