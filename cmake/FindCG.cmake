# CG: needed by old ise
#
#   CG_FOUND
#   CG_INCLUDE_DIR
#   CG_LIBRARIES
#   CG_LIBRARY_PATH
#

if (CG_INCLUDE_DIR)
  # Already in cache, be silent
  set (CG_FIND_QUIETLY TRUE)
endif ()

FIND_PATH (CG_INCLUDE_DIR "Cg/cg.h" 
  "C:/Program Files/NVIDIA Corporation/Cg/include"
  DOC "Path to CG include files")
FIND_PATH (CG_LIBRARY_PATH "cg.lib"
  "C:/Program Files/NVIDIA Corporation/Cg/lib"
  DOC "Path to CG libraries")

FIND_LIBRARY(CG_CG_LIB cg ${CG_LIBRARY_PATH})
FIND_LIBRARY(CG_CGGL_LIB cggl ${CG_LIBRARY_PATH})

if (CG_CG_LIB AND CG_CGGL_LIB)
  set (CG_LIBRARIES 
    ${CG_CG_LIB} ${CG_CGGL_LIB})
else ()
  set (CG_LIBRARIES)
endif ()

INCLUDE (FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS (CG DEFAULT_MSG 
  CG_LIBRARIES CG_INCLUDE_DIR)

MARK_AS_ADVANCED (
  CG_INCLUDE_DIR
  CG_LIBRARY_PATH
  CG_CG_LIB
  CG_CGGL_LIB)
