# Bitflow lib and include path
# These variables are set:
#
#   NIDAQ_FOUND
#   NIDAQ_INCLUDE_DIR
#   NIDAQ_LIBRARIES
#
# Only works on Windows

if (NIDAQ_LIBRARY_PATH)
  # Already in cache, be silent
  set (NIDAQ_FIND_QUIETLY TRUE)
endif ()

FIND_PATH (NIDAQ_LIBRARY_PATH "NIDAQmx.lib" 
  "C:/Program Files/National Instruments/NI-DAQ/DAQmx ANSI C Dev/lib/msvc"
  DOC "Path to NIDAQ library.")

FIND_LIBRARY (NIDAQ_LIB NIDAQmx ${NIDAQ_LIBRARY_PATH})

if (NIDAQ_LIB)
  set (NIDAQ_LIBRARIES 
    ${NIDAQ_LIB})
else ()
  set (NIDAQ_LIBRARIES)
endif ()

INCLUDE (FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS (NIDAQ DEFAULT_MSG 
  NIDAQ_LIBRARIES)

MARK_AS_ADVANCED (
  NIDAQ_LIBRARY_PATH
  NIDAQ_LIB)
