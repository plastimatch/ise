/* -------------------------------------------------------------------------*
   Copyright (c) 2004-2005 Massachusetts General Hospital
   All rights reserved.
 * -------------------------------------------------------------------------*/
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <io.h>
#include "igpax.h"
#include "vip_comm.h"
#include "ise_version.h"

/* -------------------------------------------------------------------------*
   Function prototypes
 * -------------------------------------------------------------------------*/
int igpax_init (char* client_ip, char* server_ip);
int igpax_set_mode (unsigned int mode);
void igpax_cleanup ();
void igpax_send_image (int pipe_out);
int igpax_offset_calibration (void);
int igpax_clear_corrections (void);
int igpax_start_grabbing (void);
int igpax_set_1fps (void);
int igpax_print_mode_details (int mode);
int igpax_igtalk2_corrections (void);
int igpax_set_7_5fps (void);
int igpax_set_30fps (void);

/* -------------------------------------------------------------------------*
   Global variables
 * -------------------------------------------------------------------------*/
int global_mode = 0;

/* -------------------------------------------------------------------------*
   Functions
 * -------------------------------------------------------------------------*/
int
main (int argc, char* argv[])
{
    int rc;
    int pipe_in, pipe_out;
    char *client_ip, *server_ip;
    char cmd;
    int cmd_rc = 0;

    if (argc != 5) {
        printf ("Usage: igpax client_ip server_ip pipe_in pipe_out\n");
	exit (2);
    }
    client_ip = argv[1];
    server_ip = argv[2];
    pipe_in = atoi(argv[3]);
    pipe_out = atoi(argv[4]);

    printf ("IGPAX VERSION %s\n", ISE_VERSION);

    /* Test reading and writing.  Get character 'a', and then 
	send 0 in response */
    printf ("Reading...\n");
    rc = _read (pipe_in, (char*) &cmd, sizeof(char));
    if (cmd != IGPAXCMD_TEST_PIPES) {
	printf ("Error in connection\n");
	exit (-1);
    }
    cmd_rc = 0;
    printf ("Writing...\n");
    rc = _write (pipe_out, (int*) &cmd_rc, sizeof(int));

    /* Wait for and run commands */
    while (1) {
	/* If read fails, this means pipe is closed.  Exit.. */
	printf ("Reading...\n");
	rc = read(pipe_in, (char*) &cmd, sizeof(char));
	if (rc != sizeof(char)) {
	    igpax_cleanup ();
	    exit (1);
	}
	switch (cmd) {
	case IGPAXCMD_MODE_0:
	    printf ("Trying to set mode to 0\n");
	    global_mode = 0;
	    rc = igpax_set_mode (0);
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	case IGPAXCMD_MODE_1:
	    printf ("Trying to set mode to 1\n");
	    global_mode = 1;
	    rc = igpax_set_mode (1);
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	case IGPAXCMD_MODE_2:
	    global_mode = 2;
	    rc = igpax_set_mode (2);
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	case IGPAXCMD_MODE_3:
	    global_mode = 3;
	    rc = igpax_set_mode (3);
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	case IGPAXCMD_INIT:
	    /* initialize */
	    rc = igpax_init (client_ip,server_ip);
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	case IGPAXCMD_GET_IMAGE:
	    /* get_image - return code is sent within function */
	    igpax_send_image (pipe_out);
	    break;
	case IGPAXCMD_QUIT:
	    /* quit */
	    igpax_cleanup ();
	    exit (0);
	case IGPAXCMD_OFFSET_CAL:
	    rc = igpax_offset_calibration ();
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	case IGPAXCMD_IGTALK2_CORRECTIONS:
	    rc = igpax_igtalk2_corrections ();
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	case IGPAXCMD_SET_1FPS:
	    rc = igpax_set_1fps ();
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	case IGPAXCMD_SET_7_5FPS:
	    rc = igpax_set_7_5fps ();
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	case IGPAXCMD_SET_30FPS:
	    rc = igpax_set_30fps ();
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	case IGPAXCMD_CLEAR_CORRECTIONS:
	    rc = igpax_clear_corrections ();
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	case IGPAXCMD_START_GRABBING:
	    rc = igpax_start_grabbing ();
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    break;
	default:
	    /* unknown command */
	    printf ("Unknown command.\n");
	    igpax_cleanup ();
	    exit (1);
	}
    }

    /* Never gets here */
    return 0;
}

/* First, sends return code (an int) on the pipe.  For good return
    codes, the image is also sent. */
void
igpax_send_image (int pipe_out)
{
    int x_size = 2*1024;
    int y_size = 2*768;
    int npixels = x_size * y_size;
    int rc;
    int retries = 2;
    USHORT *image_ptr;
    int complete = 0;
    int num_frames = 0;
    int num_pulses = 0;
    int ready_for_pulse = 0;

    /* check if autosense done */
    complete = 0;
    while (complete == 0) {
	rc = vip_query_progress(&num_frames, &complete, &num_pulses, &ready_for_pulse);

	if (retries-- <= 0) {
            printf ("IGPAX: no radiograph autosensed!\n");
	    rc = IGPAX_NO_RADIOGRAPH_AUTOSENSED;
	    _write (pipe_out, (int*) &rc, sizeof(int));
	    return;
	}

	// The ethernet can only be polled about once every second.
	// The following function is an example of a delay that might be used. 
	SleepEx(1000, FALSE);
    }

    // We can't call vip_get_image() while image acquisition is active
    rc = vip_sw_handshaking(VIP_SW_VALID_XRAYS, 0);
    rc = vip_sw_handshaking(VIP_SW_PREPARE, 0);

    // Get image and return to caller
    image_ptr = (USHORT *)malloc(npixels * sizeof(USHORT));
    rc = vip_get_image(2, VIP_CURRENT_IMAGE, x_size, y_size, image_ptr);
    if (rc == 0) {
	rc = _write (pipe_out, (int*) &rc, sizeof(int));
	rc = _write (pipe_out, (void*) image_ptr, npixels*sizeof(USHORT));
        printf ("Writing image: sent %d bytes\n", rc);
    } else {
	rc = IGPAX_VIP_GET_IMAGE_FAILED;
	printf ("Error: vip_get_image returned %d\n", rc);
	rc = _write (pipe_out, (int*) &rc, sizeof(int));
    }
    free (image_ptr);

    // Restart image acquisition
    rc = vip_sw_handshaking(VIP_SW_PREPARE, 1);
    if (rc != 0) {
        printf ("Error: cannot vip_sw_handshaking(VIP_SW_PREPARE, 1)\n");
	rc = IGPAX_VIP_SW_PREPARE_1;
	return;
    }

    // Check to see when the system is ready for x-rays
    while (!ready_for_pulse) {
	rc = vip_query_progress(&num_frames, &complete, &num_pulses, &ready_for_pulse);
	// The ethernet can only be polled about once every second.
	// The following function is an example of a delay that might be used. 
	SleepEx (1000, FALSE);
    }

}

int
igpax_init (char* client_ip, char* server_ip)
{
    int rc;
    int num_retries = 120;   /* 120 * 2.5 seconds = 5 mins */

    // this call is needed to make a connection with the system

    while (num_retries-- > 0) {
	printf ("IGPAX: trying link(%s,%s)\n",client_ip,server_ip);
	rc = vip_open_link(VIP_ETHERNET_LINK, client_ip, server_ip);
	if (rc == 0) {
	    break;
	} else {
	    Sleep (2500);
	}
    }
    if (rc != 0) {
        printf ("Error: cannot vip_open_link():%d\n",rc);
        return IGPAX_VIP_OPEN_LINK;
    }
    printf ("IGPAX: opened link!\n");

    /* Need to reset state when switching between radiograph 
	& fluoro */
    rc = vip_reset_state ();
    if (rc != 0) {
        printf ("Error: cannot vip_reset_state():%d\n",rc);
	return IGPAX_VIP_RESET_STATE;
    }

    return 0;
}

int
igpax_set_mode (unsigned int mode)
{
    int rc;

    if (mode == 2) {
	// setup acquisition to be auto-sense for x-rays on and off
	rc = vip_set_mode_acq_type(2, VIP_AUTO_SENSE_N_FRAMES, 1);
    }

    // select the appropriate mode
    rc = vip_select_mode(mode);
    if (rc != 0) {
        printf ("Error: cannot vip_select_mode(%d): %d\n",mode,rc );
	return IGPAX_VIP_SELECT_MODE;
    }
    return 0;
}

int
igpax_start_grabbing (void)
{
    int rc;
    int num_frames = 0;
    int complete = 0;
    int num_pulses = 0;
    int ready_for_pulse = 0;

    // GCS testing...
#if defined (commentout)
    rc = vip_sw_handshaking(VIP_SW_VALID_XRAYS, 0);
    printf ("vip_sw_handshaking(VIP_SW_VALID_XRAYS, 0): %d\n", rc);
    rc = vip_sw_handshaking(VIP_SW_PREPARE, 0);
    printf ("vip_sw_handshaking(VIP_SW_PREPARE, 0): %d\n", rc);
#endif

    // send prepare = true
    rc = vip_sw_handshaking(VIP_SW_PREPARE, 1);
    if (rc != 0) {
        printf ("Error: cannot vip_sw_handshaking(VIP_SW_PREPARE, 1):%d\n",rc);
	return IGPAX_VIP_SW_PREPARE_1;
    }

    // Check to see when the system is ready for x-rays
    while (!ready_for_pulse) {
	rc = vip_query_progress(&num_frames, &complete, &num_pulses, &ready_for_pulse);

	// The ethernet can only be polled about once every second.
	// The following function is an example of a delay that might be used. 
	SleepEx (1000, FALSE);
    }


    switch (global_mode) {
    case 0:
    case 1:
	// send xrays = true
	SleepEx (500, FALSE);
	rc = vip_sw_handshaking(VIP_SW_VALID_XRAYS, 1);
	if (rc != 0) {
	    printf ("Error: cannot vip_sw_handshaking(VIP_SW_VALID_XRAYS, 1)\n");
	    return IGPAX_VIP_SW_VALID_XRAYS_1;
	}
	igpax_print_mode_details (global_mode);
    	break;

    case 2:
	/* Return immediately with a good return code.  When caller requests
	   the image, we can return a bad code if no image present. */
#if defined (commentout)
	/* wait until autosense done */
	complete = 0;
	while (complete == 0) {
		rc = vip_query_progress(&num_frames, &complete, &num_pulses, &ready_for_pulse);

		// The ethernet can only be polled about once every second.
		// The following function is an example of a delay that might be used. 
		SleepEx(1000, FALSE);
	}
#endif
	break;
    }
    return 0;
}

int
igpax_offset_calibration (void)
{
    int rc;
    int num_frames = 0;
    int complete = 0;
    int num_pulses = 0;
    int ready_for_pulse = 0;

    /* NO LONGER hard coded for mode 2 (radiography mode) */
    rc = vip_offset_cal (global_mode);
    printf ("IGPAX: vip_offset_cal(%d) returned %d\n", global_mode, rc);
    if (rc) {
	return IGPAX_VIP_OFFSET_CAL;
    }

    // wait for the calibration to complete
    while (!complete) {
	rc = vip_query_progress(&num_frames, &complete, &num_pulses, &ready_for_pulse);
	printf ("IGPAX: offset cal frame %d %d %d %d\n", num_frames, complete,
	    num_pulses, ready_for_pulse);

	// The ethernet can only be polled about once every second.
	// The following function is an example of a delay that might be used. 
	SleepEx(1000, FALSE);
    }
    return 0;
}

int
igpax_gain_calibration (void)
{
    int rc;
    int num_frames = 0;
    int complete = 0;
    int num_pulses = 0;
    int ready_for_pulse = 0;

    rc = vip_reset_state();
    if (rc) {
	printf ("Couldn't reset state (%d).\n", rc);
	return IGPAX_VIP_RESET_STATE;
    }

    // set the number of frames to acquire during the acquisition
    rc = vip_set_num_cal_frames (global_mode, 1);

    // tell the system to prepare for a gain calibration
    rc = vip_gain_cal_prepare (global_mode);
    if (rc) {
	return IGPAX_VIP_GAIN_CAL;
    }

    // send prepare = true
    rc = vip_sw_handshaking(VIP_SW_PREPARE, 1);
    if (rc) {
	return IGPAX_VIP_GAIN_CAL;
    }

    // wait for the calibration to complete
    rc = vip_sw_handshaking(VIP_SW_VALID_XRAYS, 1);
    while (!ready_for_pulse) {
	rc = vip_query_progress(&num_frames, &complete, &num_pulses, &ready_for_pulse);
	printf ("IGPAX: gain cal frame %d %d %d %d\n", num_frames, complete,
		num_pulses, ready_for_pulse);

	// The ethernet can only be polled about once every second.
	// The following function is an example of a delay that might be used. 
	SleepEx(1000, FALSE);
    }

    // send xrays = true
    rc = vip_sw_handshaking(VIP_SW_VALID_XRAYS, 1);

    // wait for the calibration to complete
    num_frames = 0;
    while (num_frames < 1) {
	rc = vip_query_progress(&num_frames, &complete, &num_pulses, &ready_for_pulse);
	printf ("IGPAX: gain cal frame %d %d %d %d\n", num_frames, complete,
		num_pulses, ready_for_pulse);

	// The ethernet can only be polled about once every second.
	// The following function is an example of a delay that might be used. 
	SleepEx(1000, FALSE);
    }

    // send xrays = false
    rc = vip_sw_handshaking(VIP_SW_VALID_XRAYS, 0);
    // Varian example code didn't do this, but probably we should
    rc = vip_sw_handshaking(VIP_SW_PREPARE, 0);

    while (!complete) {
	rc = vip_query_progress(&num_frames, &complete, &num_pulses, &ready_for_pulse);
	printf ("IGPAX: gain cal frame %d %d %d %d\n", num_frames, complete,
		num_pulses, ready_for_pulse);

        SleepEx(1000, FALSE);
    }

    return 0;
}


/* GCS The following code makes the command processor go into a strange state, 
   where you have to reboot it. */
int
igpax_igtalk2_corrections (void)
{
    int x_size = 2*1024;
    int y_size = 2*768;
    unsigned short *img;
    int off, gain, defect, line;
    int off_new, gain_new, defect_new, line_new;
    int rc;
    BOOL enable;
    int minimum_delay, post_exposure_delay;

    int gain_median;
    double gain_sigma;
    int offset_median;
    double gain_scaling;
    long date_time;

    printf ("IGPAX: Loading correction images...\n");

    off_new = gain_new = defect_new = line_new = 0;

    img = (unsigned short*) malloc (x_size*y_size*sizeof(unsigned short));

    // disable automatic offset calibration
    rc = vip_get_auto_cal_settings (global_mode, &enable,
				    &minimum_delay, &post_exposure_delay);
    printf ("Checking automatic offset calibration settings, rc=%d\n"
	"Settings = %d, %d, %d\n", rc, enable, minimum_delay, post_exposure_delay);

#if defined (commentout)
    /* I used to do offset calibration only on the first time after reboot.  
       But it's better to always do it, so you can easily re-do in the clinic.
       Also, it doesn't take so long at 7.5 fps. */
    if (enable) {
	/* Offset calibration not yet done.  Do the offset calibration. */
	rc = vip_set_num_cal_frames (global_mode, 1);
	rc = vip_set_num_cal_frames (global_mode, 8);
	printf ("Setting # frames for offset calibration. rc=%d\n", rc);
	igpax_offset_calibration();
    }
#endif
    rc = vip_set_num_cal_frames (global_mode, 16);
    printf ("Setting # frames for offset calibration. rc=%d\n", rc);
    igpax_offset_calibration();

    /* Check the result. Was it good? */
    rc = vip_get_cal_stats (global_mode, &gain_median,
			    &gain_sigma, &offset_median, 
			    &gain_scaling, &date_time);
    printf ("Getting cal stats: rc=%d\n",rc);
    printf ("gain_median=%d\n"
	    "gain_sigma=%g\n"
	    "offset_median=%d\n"
	    "gain_scaling=%g\n"
	    "date_time=%d\n",
	    gain_median,
	    gain_sigma / 1000,
	    offset_median,
	    gain_scaling / 1000,
	    date_time);

    /* Disable automatic update of offset calibration */
    rc = vip_enable_auto_cal (global_mode, 0, 0, 0);
    printf ("Disabling of automatic offset calibration. rc=%d\n", rc);

    // Now replace with our offset calibration
#if defined (commentout)
    fp = fopen ("i1_offset_calibration.raw", "rb");
    if (fp) {
	rc = fread (img, sizeof(unsigned short), x_size*y_size, fp);
	if (rc == x_size*y_size) {
	    rc = vip_put_image (global_mode, VIP_OFFSET_IMAGE, x_size, y_size, img);
	    printf ("IGPAX: Sent offset image, rc=%d\n", rc);
	    if (rc == 0) {
		off_new = 1;
		gain_new = 1;
		defect_new = 1;
	    }
	} else {
	    printf ("IGPAX: Read offset image failed, rc=%d\n", rc);
	}
	fclose (fp);
    } else {
	printf ("IGPAX: Oops. No ofset image.\n");
    }
#endif

    /* GCS TESTING 2005/09/21: see if I can get it to work with a real offset calibration */
    off_new = 1;
    gain_new = 1;
    defect_new = 1;


    /* 2005/09/20: It's not necessary to perform a gain calibration, because
       that is stored in the CP. */
#if defined (commentout)
    // This seems to allow the gain image to go...
    //Sleep (1000);
    rc = vip_set_correction (off_new, 0, 0, 0);
    printf ("IGPAX: Sent corrections (%d,0,0,0): %d\n", off_new, rc);

    if (off_new == 1) {
	fp = fopen ("i1_gain_calibration.raw", "rb");
	if (fp) {
	    rc = fread (img, sizeof(unsigned short), x_size*y_size, fp);
	    if (rc == x_size*y_size) {
		rc = vip_put_image (global_mode, VIP_GAIN_IMAGE, x_size, y_size, img);
		printf ("IGPAX: Sent gain image, rc=%d\n", rc);
		if (rc == 0) {
		    gain_new = 1;
		}
	    } else {
		printf ("IGPAX: Read gain image failed, rc=%d\n", rc);
	    }
	    fclose (fp);
	} else {
	    printf ("IGPAX: Oops. No gain image.\n");
	}
    }
#endif

    rc = vip_get_correction (&off, &gain, &defect, &line);
    printf ("IGPAX: Correction factors were %d, %d, %d, %d\n", off, gain, defect, line);

    if (off != off_new || gain != gain_new || defect != defect_new) {
	rc = vip_set_correction (off_new, gain_new, defect_new, line_new);
	printf ("IGPAX: Setting corrections (%d,%d,%d,%d), rc=%d\n", off_new, gain_new, defect_new, line_new, rc);
	if (rc == 0) {
	    rc = vip_get_correction (&off, &gain, &defect, &line);
	    printf ("IGPAX: Correction factors are %d, %d, %d, %d\n", off, gain, defect, line);
	}
    }

    free (img);

    return 0;
}

int
igpax_clear_corrections (void)
{
    int off, gain, defect, line;
    int rc;

    rc = vip_get_correction (&off, &gain, &defect, &line);
    printf ("IGPAX: Correction factors were %d, %d, %d, %d\n", off, gain, defect, line);

    off = 0, gain = 0, defect = 0;
    rc = vip_set_correction (off, gain, defect, line);

    rc = vip_get_correction (&off, &gain, &defect, &line);
    printf ("IGPAX: Correction factors are %d, %d, %d, %d\n", off, gain, defect, line);

    return 0;
}

int
igpax_set_1fps (void)
{
    int rc;

    rc = vip_set_frame_rate (global_mode, 1.0);
    printf ("IGPAX: Setting frame rate (mode=%d, fps=1.0, rc=%d)\n", global_mode, rc);
    // igpax_print_mode_details (global_mode);

    return 0;
}

int
igpax_set_7_5fps (void)
{
    int rc;

    rc = vip_set_frame_rate (global_mode, 7.5);
    printf ("IGPAX: Setting frame rate (mode=%d, fps=7.5, rc=%d)\n", global_mode, rc);
    // igpax_print_mode_details (global_mode);

    return 0;
}

int
igpax_set_30fps (void)
{
    int rc;

    rc = vip_set_frame_rate (global_mode, 30.);
    printf ("IGPAX: Setting frame rate (mode=%d, fps=30, rc=%d)\n", global_mode, rc);
    // igpax_print_mode_details (global_mode);

    return 0;
}

int
igpax_print_mode_details (int mode)
{
    int rc;
    int mode_type, dcds_enable;
    double frame_rate, analog_gain, max_allowable_frame_rate;
    int lines_per_frame, columns_per_frame, lines_per_pixel, columns_per_pixel;
    char mode_description[64];

    rc = vip_get_mode_details(mode, 
				&mode_type, 
				&frame_rate,
				&analog_gain, 
				&lines_per_frame, 
				&columns_per_frame, 
				&lines_per_pixel, 
				&columns_per_pixel,
				mode_description, 
				&dcds_enable,
				&max_allowable_frame_rate);

    printf("\nmode data is:\n");
    printf("\nModeType is %d", mode_type);
    printf("\nFrameRate is %f", frame_rate);
    printf("\nAnalogGain is %f", analog_gain);
    printf("\nHorizontalImageSize is %d", lines_per_frame);
    printf("\nVerticalImageSize is %d", columns_per_frame);
    printf("\nLinesPerPixel is %d", lines_per_pixel);
    printf("\nColumnsPerPixel is %d", columns_per_pixel);
    printf("\nmode description is %s", mode_description);
    printf("\nDCDS enable is %d", dcds_enable);
    printf("\nMax allowable frame rate is %f\n", max_allowable_frame_rate);
    return rc;
}

void
igpax_cleanup()
{
    int rc;

    printf ("IGPAX: Cleaning up...\n");

    // send xrays = false
    rc = vip_sw_handshaking(VIP_SW_VALID_XRAYS, 0);

    // send prepare = false
    rc = vip_sw_handshaking(VIP_SW_PREPARE, 0);

    // end connection with the system
    rc = vip_close_link();
}
