//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by indico.rc
//
#define IDM_ABOUT                       104
#define IDI_INDICO                      107
#define IDI_INDICO_SMALL                108
#define IDD_INDICO_DUAL                 137
#define IDB_BITMAP1                     141
#define IDB_BITMAP2                     142
#define IDB_BITMAP3                     143
#define IDR_ACCELERATOR                 146
#define IDC_INDICO                      147
#define IDC_BUTTON1                     1028
#define IDC_RAD_FLUORO_BUTTON_1         1028
#define IDC_BUTTON2                     1029
#define IDC_RAD_FLUORO_BUTTON_2         1029
#define IDC_COMBO1                      1030
#define IDC_COMBO2                      1031
#define IDC_MESSAGE_BAR2                1032
#define IDC_COMBO3                      1033
#define IDC_COMBO4                      1034
#define IDC_COMBO5                      1035
#define IDC_COMBO6                      1036
#define IDC_IMAGE_2                     1037
#define IDC_IMAGE_1                     1038
#define IDC_MESSAGE_BAR1                1039
#define IDC_EDIT1                       1040
#define IDC_EDIT2                       1041
#define IDC_STATE_BAR_1                 1042
#define IDC_STATE_BAR_2                 1043
#define IDC_KVP_1_FEEDBACK              1044
#define IDC_KVP_2_FEEDBACK              1045
#define IDC_MA_2_FEEDBACK               1046
#define IDC_MA_1_FEEDBACK               1047
#define IDC_MS_1_FEEDBACK               1048
#define IDC_MS_2_FEEDBACK               1049
#define IDC_HE_1_FEEDBACK               1050
#define IDC_HE_2_FEEDBACK               1051
#define IDC_HH_2_FEEDBACK               1052
#define IDC_HH_1_FEEDBACK               1053
#define IDC_EDIT3                       1055
#define IDC_MS_1_EDIT                   1058
#define IDC_MA_1_EDIT                   1059
#define IDC_MS_2_EDIT                   1060
#define IDC_MS_1_SCROLL                 1061
#define IDC_MS_2_SCROLL                 1062
#define IDC_MA_1_SCROLL                 1063
#define IDC_MA_2_EDIT                   1064
#define IDC_MA_2_SCROLL                 1065
#define IDC_KVP_1_EDIT                  1066
#define IDC_KV_1_SCROLL                 1067
#define IDC_KVP_2_EDIT                  1068
#define IDC_KV_2_SCROLL                 1069
#define ID_FILE_EXIT                    32782
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        148
#define _APS_NEXT_COMMAND_VALUE         32783
#define _APS_NEXT_CONTROL_VALUE         1062
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
