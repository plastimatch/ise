/* -----------------------------------------------------------------------
   See COPYRIGHT.TXT and LICENSE.TXT for copyright and license information
   ----------------------------------------------------------------------- */
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <io.h>
#include "igpax.h"
#include "ise_version.h"
#include "ise_igpax.h"
#include "ise_error.h"

/* -----------------------------------------------------------------------
   Function prototypes
   ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
   Global variables
   ----------------------------------------------------------------------- */
char *client_ip_1, *server_ip_1;

/* -----------------------------------------------------------------------
    Public functions
   ----------------------------------------------------------------------- */
void
parse_command_line (int argc, char* argv[])
{
    if (argc != 3) {
	printf ("igtalk2 version %s\n", ISE_VERSION);
        printf ("Usage: igtalk2 mode client_ip server_ip\n");
	exit (2);
    }

    client_ip_1 = argv[1];
    server_ip_1 = argv[2];
}

int
main (int argc, char* argv[])
{
    Ise_Error rc;
    IgpaxInfo igpax_info;

    parse_command_line (argc, argv);

    /* Initialize command processors */
    ise_igpax_init ();
    rc = ise_igpax_open (&igpax_info, server_ip_1, client_ip_1);
    if (rc != ISE_SUCCESS) {
	printf ("Sorry, ise_igpax_open failed\n");
	return -1;
    }

    /* Send commands */
    rc = ise_igpax_start_fluoro (&igpax_info, 
		ISE_IMAGE_SOURCE_BITFLOW_HIRES_FLUORO, 
		ISE_FRAMERATE_7_5_FPS);

    if (rc != ISE_SUCCESS) {
	printf ("Sorry, test failed\n");
	return -1;
    }

    Sleep (INFINITE);
    return 0;
}
