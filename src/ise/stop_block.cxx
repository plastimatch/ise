/* -----------------------------------------------------------------------
   See COPYRIGHT.TXT and LICENSE.TXT for copyright and license information
   ----------------------------------------------------------------------- */
#ifdef _WIN32
#include <windows.h>
#include <io.h>
#include <process.h>
#include <direct.h>
#endif
#include <time.h>
#include <stdlib.h>
#include "ise.h"
#include "ise_framework.h"
#include "ise_globals.h"
#include "filewrite.h"
#include "debug.h"
#include "rotating.h"
#include "stop_block.h"

#include "ise_rsc.h"
#include "ise_gdi.h"
#include "ise_gl.h"
#include "ise_config.h"

#include <commctrl.h>

#define BUFLEN 1024

//#include "stdafx.h"

/* ---------------------------------------------------------------------------- *
    Global variables
 * ---------------------------------------------------------------------------- */
HANDLE*     stop_block_thread_handle;
int32       error_sb = 0;
TaskHandle  taskHandle_sb;
char        errBuff_sb[2048];

const char  chanIn[] = "myDAQ1/port2/line7";

uInt8       r_data [1];
int32       bm_read;

extern char* output_dir_base_1;
static char output_fn[_MAX_PATH], output_fn_1[_MAX_PATH];

time_t now;
struct tm *tm_now;

static FILE* sbfp = 0;
int sbp_in = 7;
int rec_time_written;

extern int complete_info_need_written;
extern double completation_percentage, extra_cycle_number;
extern char output_dir[_MAX_PATH];

#ifdef _WIN32
static CRITICAL_SECTION stop_block_cs;
#endif

/* ---------------------------------------------------------------------------- *
    Function declarations
 * ---------------------------------------------------------------------------- */
static void stop_block_thread (void* v);
static int stop_block_error (void);
static void usleep (__int64 usec);
static void compose_output_filename (char* target, char* base);
static void stop_block_printf (const char* fmt, ...);
static void stop_block_open (void);
static void stop_block_close (void);

/* ---------------------------------------------------------------------------- *
    Functions
 * ---------------------------------------------------------------------------- */
void 
stop_block_init (void)
{    
    taskHandle_sb = 0;

    //input beam on status and on block pulses
    DAQmxErrChk (DAQmxClearTask (taskHandle_sb));
    DAQmxErrChk (DAQmxCreateTask ("", &taskHandle_sb));
    DAQmxErrChk (DAQmxCreateDIChan(taskHandle_sb,chanIn,"",DAQmx_Val_ChanForAllLines));
    DAQmxErrChk (DAQmxStartTask (taskHandle_sb));
    
    stop_block_thread_handle = (HANDLE*) malloc(sizeof(HANDLE));

    if (!stop_block_thread_handle) 
    {
        MessageBox(NULL, "Stop Block thread error!", "Error!!", MB_OK);
        return;
    }

    stop_block_thread_handle[0] = (HANDLE*) _beginthread (stop_block_thread, 0, NULL);

Error:

    if (error_sb)
    {
        stop_block_error ();
    }

    return;
}


static void
stop_block_thread (void* v)
{
    int sb_recorded = 0, sbf = 0;
    
    double period = (((double)1.0/globals.mw_rate_hz)+0.002);
    double time_1 = 0, time_2 = 0, time_i = 0, timems = 0;
    int timeh = 0, timem = 0, times = 0;
    int a;
    
    LARGE_INTEGER clock_count;
    LARGE_INTEGER clock_freq;

    QueryPerformanceFrequency (&clock_freq);

    compose_output_filename (output_fn,output_dir_base_1);

    while(1)
    {
        while (globals.program_state == PROGRAM_STATE_STOPPED)
        {
            rec_time_written = 0;
            if (complete_info_need_written) {
                complete_info_need_written = 0;
                stop_block_printf ("=========================================\n");
                if (globals.rotate_platform_flag) {
                    a = (int) (globals.degree_now + 0.5);
                    stop_block_printf ("%.2f%% of acquisition completed.\n", (double) a / globals.rotate_degree_total * 100);
                }
                else {
                    stop_block_printf ("%.2f%% of acquisition completed.\n", completation_percentage);
                }
                QueryPerformanceCounter(&clock_count);
                time_2 = (double) clock_count.QuadPart / (double) clock_freq.QuadPart;
                time_i = time_2 - time_1;
                timeh = (int) time_i / 3600;
                timem = (int) time_i / 60 - timeh * 60;
                times = (int) time_i - timeh * 3600 - timem * 60;
                timems = time_i - (int) time_i;
                stop_block_printf ("Acquisition time: %02d:%02d:%.3f.\n",timeh, timem, times + timems);
                stop_block_printf ("=========================================\n\n\n");
                compose_output_filename (output_fn_1, output_dir);
                if (!MoveFileA (output_fn, output_fn_1)) {
                    MessageBox(NULL, "File stop_block_timestamp.txt move failed!", "Error!!", MB_OK);
                    rec_time_written = 0;
                }
                else
                    rec_time_written = 1;
            }
        }

        while (globals.program_state == PROGRAM_STATE_RECORDING)
        {
            while (!rec_time_written)
            {
                QueryPerformanceCounter(&clock_count);
                time_1 = (double) clock_count.QuadPart / (double) clock_freq.QuadPart;
                time (&now);
                tm_now = localtime (&now);
                
                stop_block_printf ("=========================================\n");
                stop_block_printf ("Recording starts from %04d-%02d-%02d %02d:%02d:%02d.\n",
                    tm_now->tm_year+1900, tm_now->tm_mon+1, tm_now->tm_yday, tm_now->tm_hour, tm_now->tm_min, tm_now->tm_sec);
                stop_block_printf ("Record configuration:\n");
                if (!globals.rotate_platform_flag) {
                    stop_block_printf ("  No rotation.\n");
                    stop_block_printf ("  Modulator Wheel rotational frequency: %.3f Hz.\n", globals.mw_rate_hz);
                    stop_block_printf ("  Data acquisition time will be about: %.3f sec, i.e., %.1f MW rotational periods.\n", 
                        (double) (globals.mw_cycle_number + extra_cycle_number) / globals.mw_rate_hz, (double) globals.mw_cycle_number + extra_cycle_number);
                }
                else {
                    if (globals.rotate_ctns_flag) {
                        stop_block_printf ("  Continuous rotation.\n");
                        stop_block_printf ("  Modulator Wheel rotational frequency: %6.3f Hz.\n", globals.mw_rate_hz);
                        stop_block_printf ("  About %.3f sec, i.e., %.1f MW rotational periods of data will be measured every %d degrees.\n", 
                            (double) (globals.mw_cycle_number) / globals.mw_rate_hz, 
                            (double) globals.mw_cycle_number, globals.rotate_degree_step);
                    }
                    else {
                        stop_block_printf ("  Step and shoot rotation.\n");
                        stop_block_printf ("  Modulator Wheel rotational frequency: %6.3f Hz.\n", globals.mw_rate_hz);
                        stop_block_printf ("  About %.3f sec, i.e., %.1f MW rotational periods of data will be measured every %d degrees.\n", 
                            (double) (globals.mw_cycle_number + extra_cycle_number) / globals.mw_rate_hz, 
                            (double) globals.mw_cycle_number + extra_cycle_number, globals.rotate_degree_step);
                    }
                    stop_block_printf ("  %d degrees, i.e., %d steps of data will be saved.\n", 
                        globals.rotate_degree_total, (int) globals.rotate_degree_total / globals.rotate_degree_step);
                }
                stop_block_printf ("=========================================\n");
                rec_time_written = 1;
            }
            DAQmxErrChk (DAQmxReadDigitalU8(taskHandle_sb,1,10.0,DAQmx_Val_GroupByChannel,r_data,1,&bm_read,NULL));
            //bof = (r_data[0] >> 2) & 1;
            QueryPerformanceCounter(&clock_count);
            sbf = (r_data[0] >> sbp_in) & 1;

            if ((int) sbf == 0 && !sb_recorded)
            {
                sb_recorded = 1;
            }
            
            if ((int) sbf == 1 && sb_recorded)
            {
                stop_block_printf ("%014.7f\n", (double) clock_count.QuadPart / (double) clock_freq.QuadPart);
                sb_recorded = 0;
                Sleep ((int) 900/globals.mw_rate_hz);
            }
        }
    }

Error:

    stop_block_error ();

}

int
stop_block_stop (HANDLE* stop_block_thread_handle)
{
    /* Wait for threads to finish */
    debug_printf ("StopBlock waiting for threads\n");

    WaitForMultipleObjects (1,stop_block_thread_handle,TRUE,300);
	
    debug_printf ("StopBlock threads done!\n");
    CloseHandle (stop_block_thread_handle);
    
    return 0;
}

static int
stop_block_error (void)
{
    if (DAQmxFailed (error_sb))
      DAQmxGetExtendedErrorInfo (errBuff_sb, 2048);

    if (taskHandle_sb != 0)
    {
      DAQmxStopTask (taskHandle_sb);
      DAQmxClearTask (taskHandle_sb);
    }

    if (error_sb)
    {
      debug_printf ("DAQmx Error: %d\n", error_sb);
      MessageBox(NULL, "Stop Block task error!", "Error!!", MB_OK);
    }

    return 0;
}

static void
usleep (__int64 usec)
{
    HANDLE timer;
    LARGE_INTEGER ft;
    
    ft.QuadPart = -(10*usec);

    timer = CreateWaitableTimer (NULL, TRUE, NULL);
    if (NULL == timer)
    {
        debug_printf ("Create waitable timer failed.\n");
        return;
    }
    if (!SetWaitableTimer (timer, &ft, 0, NULL, NULL, 0))
    {
        debug_printf ("Set waitable timer failed.\n");
        return;
    }
    
    if (WaitForSingleObject (timer, INFINITE) != WAIT_OBJECT_0)
    {
        debug_printf ("Wait for single object failed.\n");
        return;
    }
    CloseHandle (timer);
    return;
}

static void
compose_output_filename (char* target, char* base)
{
    sprintf (target, "%s\\stop_block_timestamp.txt", base);
}
static void
stop_block_printf (const char* fmt, ...)
{
    static int initialized = 0;
    int was_open = 1;
    va_list argptr;

    va_start (argptr, fmt);
    if (!initialized) {
	initialized = 1;
#ifdef _WIN32
	InitializeCriticalSection(&stop_block_cs);
        EnterCriticalSection(&stop_block_cs);
#endif
	if (!sbfp) {
	    was_open = 0;
	    stop_block_open();
	}
	//fprintf (sbfp, "=========================\n");
    } else {
#ifdef _WIN32
        EnterCriticalSection(&stop_block_cs);
#endif
	if (!sbfp) {
	    was_open = 0;
	    stop_block_open();
	}
    }

    vfprintf (sbfp, fmt, argptr);

    va_end (argptr);
    if (!was_open) {
	stop_block_close ();
    }

#ifdef _WIN32
    LeaveCriticalSection(&stop_block_cs);
#endif
}
static void
stop_block_open (void)
{
    char filename[_MAX_PATH];

    sprintf (filename, "%s", output_fn);
    
    if (!sbfp) {
	sbfp = fopen(filename, "a");
    }
}

static void
stop_block_close (void)
{
    if (sbfp) {
	fclose(sbfp);
	sbfp = 0;
    }
}
