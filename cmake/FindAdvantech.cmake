# Detect development libraries for Advantech I/O card.
# These variables are set:
#
#   ADVANTECH_FOUND
#   ADVANTECH_INCLUDE_DIR
#   ADVANTECH_LIBRARIES
#
# Only works on Windows

include (FindPackageHandleStandardArgs)

if (ADVANTECH_INCLUDE_DIR)
  # Already in cache, be silent
  set (Advantech_FIND_QUIETLY TRUE)
endif ()

find_path (ADVANTECH_LEGACY_DAQ_INCLUDE_DIR "Driver.h"
  "C:/Program Files/Advantech/Adsapi/Include"
  DOC "Path to ADVANTECH Legacy DAQ include files.")

if (ADVANTECH_LEGACY_DAQ_INCLUDE_DIR)
  # Older API

  set (ADVANTECH_INCLUDE_DIR "${ADVANTECH_LEGACY_DAQ_INCLUDE_DIR}")
  find_path (ADVANTECH_LIBRARY_PATH "ADSDEV.lib"
    PATHS
    "C:/Program Files/Advantech/Adsapi/Lib"
    "${ADVANTECH_LEGACY_DAQ_INCLUDE_DIR}/../Lib"
    DOC "Path to ADVANTECH libraries.")

  find_library(ADVANTECH_ADS841_LIB ADS841 ${ADVANTECH_LIBRARY_PATH})
  find_library(ADVANTECH_Adsapi32_LIB Adsapi32 ${ADVANTECH_LIBRARY_PATH})
  find_library(ADVANTECH_Adsapi32bcb_LIB Adsapi32bcb ${ADVANTECH_LIBRARY_PATH})
  find_library(ADVANTECH_Adscomm_LIB Adscomm ${ADVANTECH_LIBRARY_PATH})
  find_library(ADVANTECH_ADSDEV_LIB ADSDEV ${ADVANTECH_LIBRARY_PATH})
  find_library(ADVANTECH_adsdnet_LIB adsdnet ${ADVANTECH_LIBRARY_PATH})

  if (ADVANTECH_Adscomm_LIB AND ADVANTECH_Adsapi32_LIB)
    set (ADVANTECH_LIBRARIES ${ADVANTECH_Adscomm_LIB} ${ADVANTECH_Adsapi32_LIB})
  else ()
    set (ADVANTECH_LIBRARIES)
  endif ()

  find_package_handle_standard_args (Advantech DEFAULT_MSG 
    ADVANTECH_LIBRARIES ADVANTECH_INCLUDE_DIR)
  MARK_AS_ADVANCED (
    ADVANTECH_ADS841_LIB
    ADVANTECH_Adsapi32_LIB
    ADVANTECH_Adsapi32bcb_LIB
    ADVANTECH_Adscomm_LIB
    ADVANTECH_ADSDEV_LIB
    ADVANTECH_adsdnet_LIB)

else ()
  find_path (ADVANTECH_DAQNAVI_INCLUDE_DIR "bdaqctrl.h"
    "C:/Advantech/DAQNavi/Inc"
    DOC "Path to ADVANTECH DAQNavi include files.")

  if (ADVANTECH_DAQNAVI_INCLUDE_DIR)
    set (ADVANTECH_INCLUDE_DIR "${ADVANTECH_DAQNAVI_INCLUDE_DIR}")
  endif ()

  find_package_handle_standard_args (Advantech DEFAULT_MSG 
    ADVANTECH_INCLUDE_DIR)
endif ()
