/* -------------------------------------------------------------------------*
    See COPYRIGHT for copyright information.
 * -------------------------------------------------------------------------*/
#ifndef __ROTATE_H__
#define __ROTATE_H__

#include "VxmDriver.h"

void rotate_init (void);
int rotate_stop (HANDLE* rotate_thread_handle);

#endif