/* -------------------------------------------------------------------------*
    See COPYRIGHT for copyright information.
 * -------------------------------------------------------------------------*/
#ifndef __STOPBLOCK_H__
#define __STOPBLOCK_H__

#include "NIDAQmx.h"

#define DAQmxErrChk(functionCall) { if( DAQmxFailed(error_sb=(functionCall)) ) { goto Error; } }

extern HANDLE*     stop_block_thread_handle;
extern int32       error_sb;
extern TaskHandle  taskHandle_sb;
extern char        errBuff_sb[2048];

void stop_block_init (void);
int stop_block_stop (HANDLE* stop_block_thread_handle);


#endif