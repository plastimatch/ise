/* -------------------------------------------------------------------------*
    See COPYRIGHT for copyright information.
 * -------------------------------------------------------------------------*/
/* -------------------------------------------------------------------------*
  On serial port communication under Win32
    http://msdn2.microsoft.com/en-us/library/aa363424.aspx
    http://www.codeproject.com/system/serial_com.asp
    http://mail.python.org/pipermail/python-list/2002-November/172628.html
    I don't have conclusive evidence, but it appears that I can write to 
      the serial port in one thread, while it's reading in another.  
      This *is* explicitly allowed if Overlapped I/O is used, but haven't 
      found a clear reference for threaded apps.  See:
        http://msdn2.microsoft.com/en-us/library/ms810467.aspx
    It is not recommended to use the same event for multiple I/O requests.

  On _beginthreadex() vs. CreateThread()
    http://www.flounder.com/badprogram.htm
    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/vclib/html/_crt__beginthread.2c_._beginthreadex.asp

  Edit controls
    http://www.gamedev.net/community/forums/topic.asp?topic_id=450909

  Dialog boxes (tab advance problems)
    http://blogs.msdn.com/oldnewthing/archive/2005/04/01/404531.aspx

  Audio
    BOOL Beep(
      DWORD dwFreq,      // sound frequency, in hertz
      DWORD dwDuration   // sound duration, in milliseconds
    );

  Coloring static controls
    http://www.programmersheaven.com/mb/CandCPP/36924/36924/readmessage.aspx
    http://www.codeproject.com/dialog/TransWiz.asp
    http://www.codeguru.com/cpp/controls/editctrl/backgroundcolor/article.php/c3929/

  Current questions
    How best to communicate requests from GUI thread to worker threads?
      Append to work list queue?
    How best to handle message replies vs. new messages from 
      I should verify that response was sent
      Message replies must communicate back to GUI thread
      New messages must communicate back to GUI thread

  Notation:
    A "command" is initiated by either the generator or software
    The other device sends a "response"
    A "message" is either a command or response
    A "request" is a command initiated by the software
    An "alert" is a command initiated by the generator

  About the loss of communication with generator after 1 minute:
    Version 665 works
    Versions through 717 do not
    Version 718 works
    Version 729 works
    Version 730 will be first attempt to retrofit 717 overlapped reads
      into working code
    Version 776 still works

    Overlapped IO and Serial COMM
	http://msdn2.microsoft.com/en-us/library/ms810467.aspx
	http://alumnus.caltech.edu/~dank/overlap.htm

  Microsoft connect.c (1993 MSDN sample)
  Shows how to use PostMessage to get callback after selection 
  from listbox has been properly updated.
    http://crnarupa.singidunum.ac.yu/Crna_Rupa/godina%202004%20-%202005/Predmet%20Uvod%20u%20programiranje%20-%20Prof.%20Zoran%20Banjac%20&%20Goran%20%E6imi%86/MSDN/CD1/DN600ENU1%20(D)/SAMPLES/MSDN/TECHART/272/CONNECT.C

  Timeout Events
    Looks like timeouts events don't work as advertised with WaitForMultipleObjects
    This is apparently a win32 bug.  (Need reference)

  Very interesting bug concerning win32 named pipes here.  Never use a duplex pipe
    http://www.nedprod.com/programs/index.html

  How to get the system menu to work without window resizing
    Use WS_THICKFRAME

  Setting the icon to the system menu
    http://support.microsoft.com/kb/179582

 * -------------------------------------------------------------------------*/
#include "config.h"
#include <stdio.h>
//#define _WIN32_WINNT 0x0500
#include <windows.h>
#include <winbase.h>
#include <process.h>
#include "indico_main.h"
#include "indico_rsc.h"
//#include "ntddser.h"
#include "indico_info.h"

HINSTANCE g_hinstance;
HWND g_hwin;
HACCEL g_haccel;
DLGTEMPLATE* g_dialog_template;
HANDLE g_hbitmap_preparing;
HANDLE g_hbitmap_prepared;
HANDLE g_hbitmap_exposing;

HBRUSH g_hbrush_red = 0;

#define BUFLEN 256

//#define USE_OVERLAPPED_IO 0
#define USE_OVERLAPPED_IO 1

//#define USE_WAIT_COMM 0
#define USE_WAIT_COMM 1

//#define SERIAL_PORT_TIMEOUT INFINITE
#define SERIAL_PORT_TIMEOUT 800	    /* milliseconds */


/* Change these to disable one of the generators */
#define GEN_MIN 0
//#define GEN_MAX 1
#define GEN_MAX 2

//#define WARMUP_STAGE_1 "KV070 MA01000 MS04000"
//#define WARMUP_STAGE_3 "KV070 MA01000 MS40000"
#define WARMUP_STAGE_1 "KV070 MA00800 MS04000"
#define WARMUP_STAGE_3 "KV070 MA00800 MS40000"
#define RAD_STANDBY_A  "KV110 MA00630 MS00800"
#define RAD_STANDBY_B  "KV095 MA00630 MS00800"
#define PF_STANDBY_A  "KV110 MA00500 MS00060"
#define PF_STANDBY_B  "KV095 MA00500 MS00060"

//#define BYPASS_WARMUP 1

enum Program_Mode {
    PM_WARMUP,
    PM_RAD,
    PM_PULSED_FLUORO
};

enum Program_State {
    PS_INVALID,
    PS_INIT_ST,
    PS_INIT_REQUEST_CONFIG,
    PS_INIT_SET_CONFIG,
    PS_INIT_SET_CONFIG_RETRY,
    PS_INIT_REQUEST_TECHNIQUE,
    PS_WARMUP_1_SET_TECHNIQUE,
    PS_WARMUP_1_SET_TECHNIQUE_RETRY,
    PS_WARMUP_1,
    PS_WARMUP_2,
    PS_WARMUP_3_SET_TECHNIQUE,
    PS_WARMUP_3_SET_TECHNIQUE_RETRY,
    PS_WARMUP_3,
    PS_WARMUP_4,
    PS_RAD_STANDBY_SET_TECHNIQUE,
    PS_RAD_STANDBY_SET_TECHNIQUE_RETRY,
    PS_RAD_STANDBY,
    PS_PF_MODE_CHANGE,
    PS_PF_STANDBY_SET_TECHNIQUE,
    PS_PF_STANDBY_SET_TECHNIQUE_RETRY,
    PS_PF_STANDBY,
    PS_WARMUP_MODE_CHANGE,
};


typedef struct indico_thread_struct Indico_Thread;
struct indico_thread_struct {
    int com_no;
    HANDLE hcom;
    OVERLAPPED ov_read;
    OVERLAPPED ov_write;
    CRITICAL_SECTION write_cs;
    HANDLE write_event;
    char write_buffer[BUFLEN];	    // Queued request(s) from GUI thread
    char write_message[BUFLEN];	    // What we are writing in comm thread
    char read_byte;		    // Byte used for overlapped reads
    char read_buffer[BUFLEN];       // Raw characters from generator
    char read_message[BUFLEN];      // Most recent message from generator
    int read_buffer_len;
    HANDLE mode_change_event;	    // Program logic
    enum Program_Mode mode;	    // Program logic
    enum Program_State state;	    // Program logic
};

Indico_Thread g_indico_thread[2];

Indico_Info indico_info;


/* -------------------------------------------------------------------------*
    Shmem communication
 * -------------------------------------------------------------------------*/
#if defined (commentout)
void
init_indico_shmem ()
{
    int cmd_rc = 0;

    /* Set up shared memory */
    indico_info.h_shmem = CreateFileMapping (INVALID_HANDLE_VALUE, NULL, 
						PAGE_READWRITE, 0, sizeof (Indico_Shmem), 
						INDICO_SHMEM_STRING);
    if (!indico_info.h_shmem) {
	fprintf (stderr, "Error opening shared memory for indico\n");
	exit (1);
    }
    indico_info.shmem = (Indico_Shmem*) MapViewOfFile (indico_info.h_shmem, FILE_MAP_ALL_ACCESS, 0, 0, 0);
    if (!indico_info.shmem) {
	fprintf (stderr, "Error mapping shared memory for panel\n");
	exit (1);
    }
}
#endif

void
set_indico_shmem (int idx, int val)
{
    indico_info.shmem->rad_state[idx] = val;
}


/* -------------------------------------------------------------------------*
    GUI utilites
 * -------------------------------------------------------------------------*/
void
fetch_error_message (char* buf, int len)
{
    LPVOID lpMsgBuf;
    DWORD dw = GetLastError(); 

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );
    strncpy (buf, lpMsgBuf, len);
    buf[len-1] = 0;
    LocalFree (lpMsgBuf);
}

void
hang ()
{
    Sleep (INFINITE);
}

void
display_rad_bitmap (int control, HANDLE hbitmap)
{
    // hbitmap should be 0, or g_hbitmap_xxxx
    // control should be IDC_IMAGE_1 or IDC_IMAGE_2
    SendDlgItemMessage (
	g_hwin,
        control,
        STM_SETIMAGE,
        (WPARAM) IMAGE_BITMAP,
        (LPARAM) hbitmap);
}

/* This function mallocs a string, then sends a message to the GUI 
   thread, which displays the message in the winproc. */
void
post_status (Indico_Thread* indico, char* fmt, ...)
{
    char buf[BUFLEN];
    va_list argptr;
    va_start (argptr, fmt);

    _vsnprintf (buf, BUFLEN, fmt, argptr);
    buf[BUFLEN-1] = 0;

    va_end (argptr);

    PostMessage (g_hwin, UWM_UPDATE_MSGBAR, indico->com_no, (LPARAM) strdup(buf));
}

void
post_state (Indico_Thread* indico, char* fmt, ...)
{
    char buf[BUFLEN];
    va_list argptr;
    va_start (argptr, fmt);

    _vsnprintf (buf, BUFLEN, fmt, argptr);
    buf[BUFLEN-1] = 0;

    va_end (argptr);

    PostMessage (g_hwin, UWM_UPDATE_STATEBAR, indico->com_no, (LPARAM) strdup(buf));
}

void
post_error_message (Indico_Thread* indico, char* prefix)
{
    char buf[BUFLEN];
    fetch_error_message (buf, BUFLEN);
    post_status (indico, "%s: %s", prefix, buf);
}

void
post_kv (Indico_Thread* indico, int value)
{
    PostMessage (g_hwin, UWM_UPDATE_KVP, indico->com_no, (LPARAM) value);
}

void
post_ma (Indico_Thread* indico, int value)
{
    PostMessage (g_hwin, UWM_UPDATE_MA, indico->com_no, (LPARAM) value);
}

void
post_ms (Indico_Thread* indico, int value)
{
    PostMessage (g_hwin, UWM_UPDATE_MS, indico->com_no, (LPARAM) value);
}

void
post_align_inputs (Indico_Thread* indico)
{
    PostMessage (g_hwin, UWM_ALIGN_INPUTS, indico->com_no, (LPARAM) 0);
}

void
post_he (Indico_Thread* indico, int value)
{
    PostMessage (g_hwin, UWM_UPDATE_HE, indico->com_no, (LPARAM) value);
}

void
post_hh (Indico_Thread* indico, int value)
{
    PostMessage (g_hwin, UWM_UPDATE_HH, indico->com_no, (LPARAM) value);
}

void
post_mode (Indico_Thread* indico, int value)
{
    PostMessage (g_hwin, UWM_UPDATE_MODE_BUTTON, indico->com_no, (LPARAM) value);
}

/* -------------------------------------------------------------------------*
    Comm port routines
 * -------------------------------------------------------------------------*/
int
open_com (HANDLE* hcom, char* com_port)
{
    int rc;
    DCB dcb;
    COMMTIMEOUTS timeouts;
    BOOL fSuccess;
    DWORD dwEvtMask = 0x5555AAAA;

    /* Get HANDLE to comm port */
    *hcom = CreateFile (
	com_port,		// "COM1" or "COM2"
	GENERIC_READ | GENERIC_WRITE,  // read/write access
	0,			// must open with exclusive access
	NULL,			// default security attributes
	OPEN_EXISTING,		// must use OPEN_EXISTING
#if (USE_OVERLAPPED_IO)
	FILE_FLAG_OVERLAPPED,	// use overlapped I/O
#else
	0,			// don't use overlapped I/O
#endif
	NULL			// must be NULL
    );
    if (*hcom == INVALID_HANDLE_VALUE) {
	// Handle the error.
	return 1;
    }

    /* Set timeout values to zero (return immediately) */
#if defined (commentout)
    timeouts.ReadIntervalTimeout = MAXDWORD;
    //timeouts.ReadIntervalTimeout = 1;
    timeouts.ReadTotalTimeoutMultiplier = 0;
    timeouts.ReadTotalTimeoutConstant = 0;
    timeouts.WriteTotalTimeoutMultiplier = 0;
    timeouts.WriteTotalTimeoutConstant = 0;
#endif
    timeouts.ReadIntervalTimeout = 100;
    timeouts.ReadTotalTimeoutMultiplier = 100;
    timeouts.ReadTotalTimeoutConstant = 100;
    timeouts.WriteTotalTimeoutMultiplier = 0;
    timeouts.WriteTotalTimeoutConstant = 0;
    rc = SetCommTimeouts (*hcom, &timeouts);
    if (!rc) {
	// Handle the error
	return 2;
    }

    /* Build on the current configuration, and skip setting the size
	of the input and output buffers with SetupComm. */
    ZeroMemory (&dcb, sizeof(DCB));
    dcb.DCBlength = sizeof(DCB);
    fSuccess = GetCommState (*hcom, &dcb);
    if (!fSuccess) {
	printf ("GetCommState failed with error %d.\n", GetLastError());
	return 3;
    }

    /* Fill in DCB */
    dcb.BaudRate = CBR_19200;		// set the baud rate
    dcb.ByteSize = 8;			// data size, xmit, and rcv
    dcb.Parity = NOPARITY;		// no parity bit
    dcb.StopBits = TWOSTOPBITS;		// two stop bit
    fSuccess = SetCommState (*hcom, &dcb);
    if (!fSuccess) {
	// Handle the error.
	return 4;
    }

    rc = PurgeComm (*hcom, PURGE_TXABORT | PURGE_RXABORT | PURGE_TXCLEAR | PURGE_RXCLEAR);
    if (!rc) {
	// Handle the error
	return 5;
    }

    //rc = SetCommMask(*hcom, EV_RXCHAR);
    //rc = SetCommMask(*hcom, EV_RXCHAR | EV_ERR);
    //rc = SetCommMask(*hcom, EV_BREAK | EV_CTS | EV_DSR | EV_ERR | EV_RING | EV_RLSD | EV_RXCHAR | EV_RXFLAG | EV_TXEMPTY);
    rc = SetCommMask(*hcom, EV_CTS | EV_DSR | EV_ERR | EV_RXCHAR);
    if (!rc) {
	// Handle the error
	return 6;
    }
    /* Success */
    return 0;
}

void
setup_overlapped (OVERLAPPED *ov)
{
    ov->hEvent = CreateEvent (
	NULL,   // no security attributes 
	FALSE,  // auto reset event 
	//TRUE,   // auto reset event 
	FALSE,  // not signaled 
	NULL    // no name 
    );

    /* Intialize the rest of the OVERLAPPED structure to zero. */
    ov->Internal = 0;
    ov->InternalHigh = 0;
    ov->Offset = 0;
    ov->OffsetHigh = 0;
}

int
connect_serial_port (Indico_Thread* indico)
{
    char buf[BUFLEN];
    int rc;

    sprintf (buf, "COM%d", indico->com_no + 1);
    rc = open_com (&indico->hcom, buf);
    if (rc) {
	return rc;
    }

#if (USE_OVERLAPPED_IO)
    setup_overlapped (&indico->ov_read);
    setup_overlapped (&indico->ov_write);
    if (!indico->ov_read.hEvent || !indico->ov_write.hEvent) {
	// Handle the error
	return 7;
    }
#endif

    /* Success */
    return 0;
}

int
write_com (Indico_Thread* indico, int num_bytes)
{
    int rc;
    DWORD num_written;
    rc = WriteFile (indico->hcom, indico->write_message, num_bytes, 
		    &num_written, &indico->ov_write);
#if (USE_OVERLAPPED_IO)
    if (rc == 0) {
	if (GetLastError() == ERROR_IO_PENDING) {
	    BOOL rc;
	    int num_written;
	    rc = GetOverlappedResult (indico->hcom, &indico->ov_write,
					&num_written, FALSE);
	    return rc;
	} else {
	    post_error_message (indico, "WriteFile returned error ");
	    return 1;
	}
    }
#endif
    if (num_written == 0) {
	return 1;
    }
    /* Success */
    return 0;
}

/* Appends end of transmission and checksum, so bytes[] needs to be 
   at least num_bytes + 2 long */
void
append_write_checksum (char* bytes, int num_bytes)
{
    int i;
    char extra = 0x03;
    bytes[num_bytes] = extra;
    for (i = 0; i < num_bytes; i++) {
	extra += bytes[i]; /* Truncate to byte on overflow */
    }
    bytes[num_bytes+1] = extra;
    bytes[num_bytes+2] = 0; /* zero termination helps debugging */
}

/* GCS FIX: Return code is ignored */
int
send_message (Indico_Thread* indico)
{
    int num_bytes = strlen(indico->write_message);
    append_write_checksum (indico->write_message, num_bytes);
    return write_com (indico, num_bytes+2);
}

void
send_request (Indico_Thread* indico, char* fmt, ...)
{
    va_list argptr;
    va_start (argptr, fmt);

    _vsnprintf (indico->write_message, BUFLEN, fmt, argptr);
    indico->write_message[BUFLEN-1] = 0;

    va_end (argptr);

    send_message (indico);
}

/* Read a single byte from com. Return 1 if waiting on read. */
int
read_com (Indico_Thread* indico)
{
    int rc;
    DWORD num_read;
#if (USE_OVERLAPPED_IO)
retry_read:
    rc = ReadFile (indico->hcom, &indico->read_byte, 1, &num_read, &indico->ov_read);
    if (rc == 0) {
	/* Function did not succeed */
	if (GetLastError() == ERROR_IO_PENDING) {
	    return 1;
	} else {
	    post_error_message (indico, "ReadFile returned error ");
	    return 0;
	}
    }
    if (num_read == 0) {
	goto retry_read;
    }
#else
    rc = ReadFile (indico->hcom, &indico->read_byte, 1, &num_read, 0);
    if (num_read == 0) {
	return 1;
    }
#endif
    /* Success */
    return 0;
}

/* Keep reading until no more bytes. Return 1 if waiting on read. */
int
read_com_loop (Indico_Thread* indico)
{
    while (1) {
	int rc;
	rc = read_com (indico);
	if (rc) {
	    return 1;
	}
	indico->read_buffer[indico->read_buffer_len] = indico->read_byte;
	if (++(indico->read_buffer_len) >= BUFLEN) {
	    post_status (indico, "Fatal error: read buffer overflow");
	    hang();
	}
    }
    return 0;
}

/* Async read completed, return 1 if byte was read. */
int
read_com_overlapped (Indico_Thread* indico)
{
    BOOL rc;
    int num_read;

    rc = GetOverlappedResult (indico->hcom, &indico->ov_read, &num_read, FALSE);
    if (rc == 0) {
	/* Function did not succeed */
	if (GetLastError() == ERROR_OPERATION_ABORTED) {
	    post_status (indico, "Fatal error, overlapped read: ERROR_OPERATION_ABORTED");
	} else {
	    post_error_message (indico, "Fatal error, overlapped read");
	}
    } else {
	if (num_read == 1) {
	    indico->read_buffer[indico->read_buffer_len] = indico->read_byte;
	    if (++(indico->read_buffer_len) >= BUFLEN) {
		post_status (indico, "Fatal error: read buffer overflow");
		hang();
	    }
	    return 1;
	}
    }
    return 0;
}

/* -------------------------------------------------------------------------*
    Reader thread logic
 * -------------------------------------------------------------------------*/
void
process_er (Indico_Thread* indico, int i)
{
    sprintf (indico->write_message, "ER%03d", i);
    send_message (indico);
}

void
process_alert_pr (Indico_Thread* indico, int value)
{
    int control;

    if (indico->com_no == 0) {
	control = IDC_IMAGE_1;
    } else {
	control = IDC_IMAGE_2;
    }
    switch (value) {
    case 0:
	display_rad_bitmap (control, 0);
	break;
    case 1:
	display_rad_bitmap (control, g_hbitmap_preparing);
	break;
    case 2:
	display_rad_bitmap (control, g_hbitmap_prepared);
	break;
    default:
        post_status (indico, "Bad value for msg PR: %d", value);
	hang ();
    }
}

void
process_alert_xr (Indico_Thread* indico, int value)
{
    int control;

    if (indico->com_no == 0) {
	control = IDC_IMAGE_1;
    } else {
	control = IDC_IMAGE_2;
    }
    switch (value) {
    case 0:
	display_rad_bitmap (control, 0);
	break;
    case 1:
	display_rad_bitmap (control, g_hbitmap_exposing);
	break;
    default:
        post_status (indico, "Bad value for msg XR: %d", value);
	hang ();
    }
}

/* Return 1 if it was a generator-initiated message */
int
process_alert (Indico_Thread* indico)
{
    int i1;

    /* Messages initiated by generator */
    if (sscanf (indico->read_message, "ER%d", &i1) == 1) {
	send_request (indico, indico->read_message);
	post_status (indico, "Alert: %s", indico->read_message);
	return 1;
    }
    else if (sscanf (indico->read_message, "EL%d", &i1) == 1) {
	/* Do nothing */
	send_request (indico, indico->read_message);
	post_status (indico, "Alert: %s", indico->read_message);
	return 1;
    }
    else if (sscanf (indico->read_message, "PR%d", &i1) == 1) {
	process_alert_pr (indico, i1);    // i1 = 0, 1, or 2
	if (i1 == 0) {
	    set_indico_shmem (indico->com_no, INDICO_SHMEM_XRAY_OFF);
	} else if (i1 == 1) {
	    set_indico_shmem (indico->com_no, INDICO_SHMEM_XRAY_SPINNING_UP);
	} else if (i1 == 2) {
	    set_indico_shmem (indico->com_no, INDICO_SHMEM_XRAY_PREPARED);
	} else {
	    post_status (indico, "Illegal alert: PR%d", i1);
	    hang ();
	}
	send_request (indico, indico->read_message);
	post_status (indico, "Alert: %s", indico->read_message);
	return 1;
    }
    else if (sscanf (indico->read_message, "XR%d", &i1) == 1) {
	process_alert_xr (indico, i1);  // i1 = 0 or 1
	if (i1 == 0) {
	    set_indico_shmem (indico->com_no, INDICO_SHMEM_XRAY_OFF);
	} else if (i1 == 1) {
	    set_indico_shmem (indico->com_no, INDICO_SHMEM_XRAY_ON);
	} else {
	    post_status (indico, "Illegal alert: XR%d", i1);
	    hang ();
	}
	send_request (indico, indico->read_message);
	post_status (indico, "Alert: %s", indico->read_message);
	return 1;
    }
    else if (sscanf (indico->read_message, "FLP%d", &i1) == 1) {
	process_alert_pr (indico, i1);
	if (i1 == 0) {
	    set_indico_shmem (indico->com_no, INDICO_SHMEM_XRAY_OFF);
	} else if (i1 == 1) {
	    set_indico_shmem (indico->com_no, INDICO_SHMEM_XRAY_SPINNING_UP);
	} else {
	    post_status (indico, "Illegal alert: FLP%d", i1);
	    hang ();
	}
	send_request (indico, indico->read_message);
	post_status (indico, "Alert: %s", indico->read_message);
	return 1;
    }
    else if (sscanf (indico->read_message, "FLX%d", &i1) == 1) {
	process_alert_xr (indico, i1);
	if (i1 == 0) {
	    set_indico_shmem (indico->com_no, INDICO_SHMEM_XRAY_OFF);
	} else if (i1 == 1) {
	    set_indico_shmem (indico->com_no, INDICO_SHMEM_XRAY_ON);
	} else {
	    post_status (indico, "Illegal alert: XR%d", i1);
	    hang ();
	}
	send_request (indico, indico->read_message);
	post_status (indico, "Alert: %s", indico->read_message);
	return 1;
    }

    return 0;
}

/* Return 1 if message stored in read_message */
int
dequeue_response (Indico_Thread* indico)
{
    int i, j;

    for (i = 0; i < indico->read_buffer_len; i++) {
	/* Indico can send 2 messages separated by a space */
	if (indico->read_buffer[i] == 0x03) {
	    /* This is the end of message case */
	    memcpy (indico->read_message, indico->read_buffer, i*sizeof(char));
	    indico->read_message[i] = 0;
	    for (j = i+2; j < indico->read_buffer_len; j++) {
		indico->read_buffer[j-i-2] = indico->read_buffer[j];
	    }
	    indico->read_buffer_len -= i+2;
	    indico->read_buffer[indico->read_buffer_len] = 0;	/* Zero termination helps debugging */
	    return 1;
	} else if (indico->read_buffer[i] == 0x20) {
	    /* This is the space separation case */
	    memcpy (indico->read_message, indico->read_buffer, i*sizeof(char));
	    indico->read_message[i] = 0;
	    for (j = i+1; j < indico->read_buffer_len; j++) {
		indico->read_buffer[j-i-1] = indico->read_buffer[j];
	    }
	    indico->read_buffer_len -= i+1;
	    indico->read_buffer[indico->read_buffer_len] = 0;	/* Zero termination helps debugging */
	    return 1;
	}
    }
    return 0;
}

void
process_request_response (Indico_Thread* indico, int alert)
{
    int i1;
    switch (indico->state) {
    case PS_INIT_ST:
	if (alert) break;
	if (sscanf (indico->read_message, "ST%d", &i1) != 1) {
	    goto wrong_message;
	}
	if (i1 == 1) {
	    /* Need to keep polling */
	    send_request (indico, "ST");
	} else if (i1 == 2) {
	    post_status (indico, "Reset fluoro");
	    indico->state = PS_INIT_REQUEST_CONFIG;
	    send_request (indico, "ET? FLF? FLU? FO? CT?");
	} else if (i1 == 6 || i1 == 9) {
	    post_status (indico, "Re-initializing");
	    send_request (indico, "ST");
	} else {
	    post_status (indico, "Initialization failed, status code %d", i1);
	    hang ();
	}
	break;
    case PS_INIT_REQUEST_CONFIG:
	if (alert) break;
	if (sscanf (indico->read_message, "ET%d", &i1) == 1) {
	    /* Do nothing */
	}
	else if (sscanf (indico->read_message, "FLF%d", &i1) == 1) {
	    /* Do nothing */
	}
	else if (sscanf (indico->read_message, "FLU%d", &i1) == 1) {
	    /* Do nothing */
	}
	else if (sscanf (indico->read_message, "FO%d", &i1) == 1) {
	    /* Do nothing */
	}
	else if (sscanf (indico->read_message, "CT%d", &i1) == 1) {
	    indico->state = PS_INIT_SET_CONFIG;
	    send_request (indico, "ET0 FLF0 CT0");
	}
	else {
	    goto wrong_message;
	}
	break;
    case PS_INIT_SET_CONFIG:
    case PS_INIT_SET_CONFIG_RETRY:
	if (alert) break;
	if (sscanf (indico->read_message, "ET%d", &i1) == 1) {
	    /* Do nothing */
	}
	else if (sscanf (indico->read_message, "FLF%d", &i1) == 1) {
	    if (i1 != 0) {
		indico->state = PS_INIT_SET_CONFIG_RETRY;
	    }
	}
	else if (sscanf (indico->read_message, "FLU%d", &i1) == 1) {
	    /* Do nothing */
	}
	else if (sscanf (indico->read_message, "FO%d", &i1) == 1) {
	    /* Do nothing */
	}
	else if (sscanf (indico->read_message, "CT%d", &i1) == 1) {
	    if (indico->state == PS_INIT_SET_CONFIG_RETRY) {
		post_status (indico, "Setting config (retry)");
		indico->state = PS_INIT_SET_CONFIG;
		send_request (indico, "ET0 FLF0 CT0");
	    } else {
		post_status (indico, "Requesting technique");
		indico->state = PS_INIT_REQUEST_TECHNIQUE;
		send_request (indico, "RR");
	    }
	}
	else {
	    goto wrong_message;
	}
	break;
    case PS_INIT_REQUEST_TECHNIQUE:
	if (alert) break;
	if (sscanf (indico->read_message, "KV%d", &i1) == 1) {
	    post_kv (indico, i1);
	}
	else if (sscanf (indico->read_message, "MA%d", &i1) == 1) {
	    post_ma (indico, i1/10);
	}
	else if (sscanf (indico->read_message, "MS%d", &i1) == 1) {
	    post_ms (indico, i1/10);
	}
	else if (sscanf (indico->read_message, "MX%d", &i1) == 1) {
	    post_status (indico, "Setting technique");
	    post_align_inputs (indico);
#if defined (BYPASS_WARMUP)
	    post_state (indico, "Rad set technique");
	    indico->state = PS_RAD_STANDBY_SET_TECHNIQUE;
	    if (indico->com_no == 0) {
		send_request (indico, RAD_STANDBY_A);
	    } else {
		send_request (indico, RAD_STANDBY_B);
	    }
#else
	    post_state (indico, "Warmup set technique");
	    indico->state = PS_WARMUP_1_SET_TECHNIQUE;
	    send_request (indico, WARMUP_STAGE_1);
#endif
	}
	else {
	    goto wrong_message;
	}
	break;
    case PS_WARMUP_1_SET_TECHNIQUE:
    case PS_WARMUP_1_SET_TECHNIQUE_RETRY:
	if (alert) break;
	if (sscanf (indico->read_message, "KV%d", &i1) == 1) {
	    post_kv (indico, i1);
	}
	else if (sscanf (indico->read_message, "MA%d", &i1) == 1) {
	    post_ma (indico, i1/10);
	}
	else if (sscanf (indico->read_message, "MS%d", &i1) == 1) {
	    post_ms (indico, i1/10);
	    post_align_inputs (indico);
	    if (indico->state == PS_WARMUP_1_SET_TECHNIQUE_RETRY) {
		post_status (indico, "Setting technique (retry)");
		indico->state = PS_WARMUP_1_SET_TECHNIQUE;
		send_request (indico, WARMUP_STAGE_1);
	    } else {
		post_state (indico, "Warmup stage 1");
		indico->state = PS_WARMUP_1;
	    }
	} else {
	    goto wrong_message;
	}
	break;
    case PS_WARMUP_1:
	if (sscanf (indico->read_message, "PR%d", &i1) == 1) {
	    if (i1 == 0) {
		post_state (indico, "Warmup stage 2");
		indico->state = PS_WARMUP_2;
	    }
	}
	break;
    case PS_WARMUP_2:
	if (sscanf (indico->read_message, "PR%d", &i1) == 1) {
	    if (i1 == 0) {
		post_state (indico, "Warmup stage 3");
		post_align_inputs (indico);
		indico->state = PS_WARMUP_3_SET_TECHNIQUE;
		send_request (indico, WARMUP_STAGE_3);
	    }
	}
	break;
    case PS_WARMUP_3_SET_TECHNIQUE:
    case PS_WARMUP_3_SET_TECHNIQUE_RETRY:
	if (alert) break;
	if (sscanf (indico->read_message, "KV%d", &i1) == 1) {
	    post_kv (indico, i1);
	}
	else if (sscanf (indico->read_message, "MA%d", &i1) == 1) {
	    post_ma (indico, i1/10);
	}
	else if (sscanf (indico->read_message, "MS%d", &i1) == 1) {
	    post_ms (indico, i1/10);
	    post_align_inputs (indico);
	    if (indico->state == PS_WARMUP_3_SET_TECHNIQUE_RETRY) {
		post_state (indico, "Setting technique (retry)");
		indico->state = PS_WARMUP_3_SET_TECHNIQUE;
		send_request (indico, WARMUP_STAGE_3);
	    } else {
		post_state (indico, "Warmup stage 3");
		indico->state = PS_WARMUP_3;
	    }
	} else {
	    goto wrong_message;
	}
	break;
    case PS_WARMUP_3:
	if (sscanf (indico->read_message, "PR%d", &i1) == 1) {
	    if (i1 == 0) {
		post_state (indico, "Warmup stage 4");
		post_align_inputs (indico);
		indico->state = PS_WARMUP_4;
	    }
	}
	break;
    case PS_WARMUP_4:
	if (sscanf (indico->read_message, "PR%d", &i1) == 1) {
	    if (i1 == 0) {
		post_state (indico, "Rad Standby");
		post_align_inputs (indico);
		indico->state = PS_RAD_STANDBY_SET_TECHNIQUE;
		if (indico->com_no == 0) {
		    send_request (indico, RAD_STANDBY_A);
		} else {
		    send_request (indico, RAD_STANDBY_B);
		}
	    }
	}
	break;
    case PS_RAD_STANDBY_SET_TECHNIQUE:
    case PS_RAD_STANDBY_SET_TECHNIQUE_RETRY:
	if (alert) break;
	if (sscanf (indico->read_message, "KV%d", &i1) == 1) {
	    post_kv (indico, i1);
	}
	else if (sscanf (indico->read_message, "MA%d", &i1) == 1) {
	    post_ma (indico, i1/10);
	}
	else if (sscanf (indico->read_message, "MS%d", &i1) == 1) {
	    post_ms (indico, i1/10);
	    post_align_inputs (indico);
	    if (indico->state == PS_RAD_STANDBY_SET_TECHNIQUE_RETRY) {
		post_state (indico, "Setting technique (retry)");
		indico->state = PS_RAD_STANDBY_SET_TECHNIQUE;
		if (indico->com_no == 0) {
		    send_request (indico, RAD_STANDBY_A);
		} else {
		    send_request (indico, RAD_STANDBY_B);
		}
	    } else {
		post_state (indico, "Rad Standby");
		indico->state = PS_RAD_STANDBY;
		indico->mode = PM_RAD;
		post_mode (indico, indico->mode);
	    }
	} else {
	    goto wrong_message;
	}
	break;
    case PS_RAD_STANDBY:
	if (alert) break;
	if (sscanf (indico->read_message, "HE%d", &i1) == 1) {
	    post_he (indico, i1);
	}
	else if (sscanf (indico->read_message, "HH%d", &i1) == 1) {
	    post_hh (indico, i1);
	}
	else if (sscanf (indico->read_message, "ST%d", &i1) == 1) {
	    post_status (indico, "ST = %d", i1);
	}
	else if (sscanf (indico->read_message, "FLF%d", &i1) == 1) {
	    post_status (indico, "FLF = %d", i1);
	    if (i1 != 0) {
		send_request (indico, "FLF0");
	    }
	}
	else if (sscanf (indico->read_message, "KV%d", &i1) == 1) {
	    post_kv (indico, i1);
	    post_state (indico, "Rad Standby");
	}
	else if (sscanf (indico->read_message, "MA%d", &i1) == 1) {
	    post_ma (indico, i1/10);
	    post_state (indico, "Rad Standby");
	}
	else if (sscanf (indico->read_message, "MS%d", &i1) == 1) {
	    post_ms (indico, i1/10);
	    post_state (indico, "Rad Standby");
	}
	else {
	    goto wrong_message;
	}
	break;
    case PS_PF_MODE_CHANGE:
	if (alert) break;
	if (sscanf (indico->read_message, "HE%d", &i1) == 1) {
	    post_he (indico, i1);
	}
	else if (sscanf (indico->read_message, "HH%d", &i1) == 1) {
	    post_hh (indico, i1);
	}
	else if (sscanf (indico->read_message, "ST%d", &i1) == 1) {
	    post_status (indico, "ST = %d", i1);
	}
	else if (sscanf (indico->read_message, "FLF%d", &i1) == 1) {
	    post_status (indico, "FLF = %d", i1);
	    if (i1 != 2) {
		send_request (indico, "FLF2");
	    } else {
		send_request (indico, "RF FLW?");
		indico->state = PS_PF_STANDBY;
	    }
	}
	else {
	    goto wrong_message;
	}
	break;
    case PS_PF_STANDBY_SET_TECHNIQUE:
    case PS_PF_STANDBY_SET_TECHNIQUE_RETRY:
	if (alert) break;
	if (sscanf (indico->read_message, "KV%d", &i1) == 1) {
	    post_kv (indico, i1);
	}
	else if (sscanf (indico->read_message, "MA%d", &i1) == 1) {
	    post_ma (indico, i1/10);
	}
	else if (sscanf (indico->read_message, "MS%d", &i1) == 1) {
	    post_ms (indico, i1/10);
	    post_align_inputs (indico);
	    if (indico->state == PS_PF_STANDBY_SET_TECHNIQUE_RETRY) {
		post_state (indico, "Setting technique (retry)");
		indico->state = PS_PF_STANDBY_SET_TECHNIQUE;
		if (indico->com_no == 0) {
		    send_request (indico, PF_STANDBY_A);
		} else {
		    send_request (indico, PF_STANDBY_B);
		}
	    } else {
		post_state (indico, "PF Standby");
		indico->state = PS_PF_STANDBY;
	    }
	} else {
	    goto wrong_message;
	}
	break;
    case PS_PF_STANDBY:
	if (alert) break;
	if (sscanf (indico->read_message, "HE%d", &i1) == 1) {
	    post_he (indico, i1);
	}
	else if (sscanf (indico->read_message, "HH%d", &i1) == 1) {
	    post_hh (indico, i1);
	}
	else if (sscanf (indico->read_message, "ST%d", &i1) == 1) {
	    post_status (indico, "ST = %d", i1);
	}
	else if (sscanf (indico->read_message, "FLF%d", &i1) == 1) {
	    post_status (indico, "FLF = %d", i1);
	    if (i1 != 2) {
		send_request (indico, "FLF2");
	    }
	}
	else if (sscanf (indico->read_message, "FLK%d", &i1) == 1) {
	    post_kv (indico, i1);
	    post_align_inputs (indico);
	    post_state (indico, "PF Standby");
	}
	else if (sscanf (indico->read_message, "FLM%d", &i1) == 1) {
	    post_ma (indico, i1/10);
	    post_align_inputs (indico);
	    post_state (indico, "PF Standby");
	}
	else if (sscanf (indico->read_message, "FLW%d", &i1) == 1) {
	    post_ms (indico, i1);
	    post_align_inputs (indico);
	    post_state (indico, "PF Standby");
	}
	else if (sscanf (indico->read_message, "FLI%d", &i1) == 1) {
	}
	else if (sscanf (indico->read_message, "FLT%d", &i1) == 1) {
	}
	else if (sscanf (indico->read_message, "FLA%d", &i1) == 1) {
	}
	else if (sscanf (indico->read_message, "FLS%d", &i1) == 1) {
	}
	else if (sscanf (indico->read_message, "FLZ%d", &i1) == 1) {
	}
	else if (sscanf (indico->read_message, "FLD%d", &i1) == 1) {
	}
	else {
	    goto wrong_message;
	}
	break;
    case PS_WARMUP_MODE_CHANGE:
	if (alert) break;
	if (sscanf (indico->read_message, "HE%d", &i1) == 1) {
	    post_he (indico, i1);
	}
	else if (sscanf (indico->read_message, "HH%d", &i1) == 1) {
	    post_hh (indico, i1);
	}
	else if (sscanf (indico->read_message, "ST%d", &i1) == 1) {
	    post_status (indico, "ST = %d", i1);
	}
	else if (sscanf (indico->read_message, "FLF%d", &i1) == 1) {
	    post_status (indico, "FLF = %d", i1);
	    if (i1 != 0) {
		send_request (indico, "FLF0");
	    } else {
		post_state (indico, "Warmup set technique");
		indico->state = PS_WARMUP_1_SET_TECHNIQUE;
		send_request (indico, WARMUP_STAGE_1);
	    }
	}
	else {
	    goto wrong_message;
	}
	break;
    default:
	post_status (indico, "Fatal error: illegal state");
	hang ();
    }
    return;

wrong_message:
    post_status (indico, "Fatal error: illegal message (%d,%s)", 
	indico->state, indico->read_message);
    hang ();
}

void
process_read_event (Indico_Thread* indico)
{
    while (dequeue_response (indico)) {
	int alert = process_alert (indico);
	process_request_response (indico, alert);
    }
}

void
handle_write_buffer (Indico_Thread* indico)
{
    /* Called by comm thread */
    EnterCriticalSection (&indico->write_cs);
    strcpy (indico->write_message, indico->write_buffer);
    indico->write_buffer[0] = 0;
    LeaveCriticalSection (&indico->write_cs);
    send_message (indico);
}

/* Called by GUI thread */
void
make_write_buffer (Indico_Thread* indico, char* fmt, ...)
{
    va_list argptr;
    char request[BUFLEN];

    va_start (argptr, fmt);
    _vsnprintf (request, BUFLEN, fmt, argptr);
    request[BUFLEN-1] = 0;
    va_end (argptr);

    EnterCriticalSection (&indico->write_cs);
    if (indico->write_buffer[0]) {
	int curlen = strlen (indico->write_buffer);
	int newlen = strlen (request);
	if (curlen+newlen+4 > BUFLEN) {
	    post_status (indico, "Fatal error: write request overflow");
	    hang ();
	}
	strcat (indico->write_buffer, " ");
	strcat (indico->write_buffer, request);
    } else {
	strcpy (indico->write_buffer, request);
    }
    LeaveCriticalSection (&indico->write_cs);
    SetEvent (indico->write_event);
}

void
handle_read_event (Indico_Thread* indico)
{
    DWORD mask;
    if (GetCommMask (indico->hcom, &mask)) {
	if (mask & EV_DSR) {
	    // post_status (indico, "Fatal error: EV_DSR");
	    // hang ();
	}
	else if (mask & EV_CTS) {
	    post_status (indico, "Fatal error: EV_CTS");
	    hang ();
	}
	else if (mask & EV_RXCHAR) {
	    while (1) {
		int rc;
		rc = read_com (indico);
		if (rc) {
		    return; /* No more bytes */
		}
		if (++(indico->read_buffer_len) >= BUFLEN) {
		    post_status (indico, "Fatal error: read buffer overflow");
		    hang();
		}
	    }
	}
	else {
	    post_status (indico, "Fatal error: unknown read event");
	    hang ();
	}
    } else {
	post_status (indico, "Fatal error: GetCommMask failed");
	hang ();
    }
}

/* if initializing is true, this function waits for a single read response */
void
reader_wait_for_event (Indico_Thread* indico, int initializing)
{
    int num_objects;
    HANDLE wait_handles[4];
    OVERLAPPED ov_status;
    DWORD dwEvtMask = 0;
    DWORD wmo_rc;
    int waiting_on_read = 0;
    int waiting_on_status = 0;
    BOOL rc;
    DWORD event;

    setup_overlapped (&ov_status);

#if (USE_OVERLAPPED_IO)
    wait_handles[0] = ov_status.hEvent;
    wait_handles[1] = indico->ov_read.hEvent;
    wait_handles[2] = indico->write_event;            /* Write request from GUI */
    wait_handles[3] = indico->mode_change_event;      /* Mode change request from GUI */
    num_objects = 4;
#else
    wait_handles[0] = ov_status.hEvent;
    num_objects = 1;
#endif /* USE_OVERLAPPED_IO */


    /* Block, reading and processing */
    while (1) {
	/* Request status */
	if (!waiting_on_status) {
	    rc = WaitCommEvent (indico->hcom, &event, &ov_status);
	    if (!rc) {
		if (GetLastError() == ERROR_IO_PENDING) {
		    waiting_on_status = 1;
		} else {
		    /* Some kind of error */
		    post_error_message (indico, "Warning [WaitCommEvent failed 1] ");
		    continue;
		}
	    } else {
		/* I have no idea what this means. */
		post_error_message (indico, "Warning [WaitCommEvent failed 2] ");
		Sleep (100);
		continue;
	    }
	}

	/* Request data */
	if (!waiting_on_read) {
	    read_com_loop (indico);
	    process_read_event (indico);
	    waiting_on_read = 1;
	}

	/* Block until event (e.g. status or read) is available */
        wmo_rc = WaitForMultipleObjects (num_objects, wait_handles, FALSE, SERIAL_PORT_TIMEOUT);
        switch (wmo_rc) {
        case WAIT_OBJECT_0:
	    waiting_on_status = 0;
	    break;
        case WAIT_OBJECT_0 + 1:
	    rc = read_com_overlapped (indico);
	    waiting_on_read = 0;
	    if (rc == 1) {
		process_read_event (indico);
	    }
	    if (indico->state == PS_RAD_STANDBY || indico->state == PS_PF_STANDBY) {
		static int ping = 0;
		if (++ping == 5) {
		    send_request (indico, "HE? HH?");
		    ping = 0;
		}
	    }

	    break;
        case WAIT_OBJECT_0 + 2:
	    /* This is for write requests from the GUI thread. 
		There can only be one at a time. */
	    post_status (indico, "Write request");
	    handle_write_buffer (indico);
	    break;
        case WAIT_OBJECT_0 + 3:
	    /* This is for mode change requests from the GUI thread. */
	    /* GCS FIX:
	       Technically we should request the state diagram logic 
	       perform the state change, because the state might not 
	       be standby (which are robust to change of mode).
	       But that will take longer to implement */
	    if (indico->mode == PM_PULSED_FLUORO) {
		post_state (indico, "PF Mode change");
		indico->state = PS_PF_MODE_CHANGE;
		send_request (indico, "FLF2");
		post_status (indico, "Mode change request: PF");
		post_align_inputs (indico);
	    } else if (indico->mode == PM_WARMUP) {
		post_state (indico, "PF Mode change");
		indico->state = PS_WARMUP_MODE_CHANGE;
		send_request (indico, "FLF2");
		post_status (indico, "Mode change request: Warmup");
		post_align_inputs (indico);
	    } else {
		/* Rad */
		post_state (indico, "Rad standby");
		indico->state = PS_RAD_STANDBY;
		send_request (indico, "FLF0");
		post_status (indico, "Mode change request: Rad");
		post_align_inputs (indico);
	    }
	    post_mode (indico, indico->mode);
	    break;
#if defined (commentout)
	case WAIT_OBJECT_0 + 3:
	    if (indico->state == PS_RAD_STANDBY) {
		send_request (indico, "ST? HE? HH?");
	    }
	    break;
	case WAIT_TIMEOUT:
	    //post_status (indico, "WAIT_TIMEOUT");
	    if (indico->state == PS_RAD_STANDBY) {
		static int ping = 1;
		if (ping = (!ping)) {
		    send_request (indico, "ST");
		} else {
		    send_request (indico, "HE? HH?");
		}
	    }
	    break;
#endif
	case WAIT_FAILED:
	    post_error_message (indico, "Wait failed: ");
	    break;
        }
    }
}

void
indico_reader_thread (Indico_Thread* indico)
{
    int rc;

    post_status (indico, "Hello world");

    /* Initialize connection to serial port */
    rc = connect_serial_port (indico);
    if (rc != 0) {
	char buf[BUFLEN];
	fetch_error_message (buf, BUFLEN);
	post_status (indico, "Connect serial failed: %d (%s)", rc, buf);
	hang ();
    }

    /* Send initialization message */
    post_status (indico, "Initializing");
    indico->state = PS_INIT_ST;
    send_request (indico, "ST");

    /* Main loop */
    reader_wait_for_event (indico, 0);
}

void
create_write_buffer_event (Indico_Thread* indico)
{
    indico->write_event = CreateEvent (
	NULL,   // no security attributes 
	FALSE,  // auto reset event 
	FALSE,  // not signaled 
	NULL    // no name 
    );
    if (indico->write_event == 0) {
	/* GCS FIX: What to do??? */
    }
}

void
create_mode_change_event (Indico_Thread* indico)
{
    indico->mode_change_event = CreateEvent (
	NULL,   // no security attributes 
	FALSE,  // auto reset event 
	FALSE,  // not signaled 
	NULL    // no name 
    );
    if (indico->mode_change_event == 0) {
	/* GCS FIX: What to do??? */
    }
}

void
create_indico_threads ()
{
    int i;
    for (i = GEN_MIN; i < GEN_MAX; i++) {
	/* Initialize indico data structure */
	Indico_Thread* indico = &g_indico_thread[i];
	indico->mode = PM_WARMUP;
	/* GCS 2010-12-29 
	   Originally : i = (0,1) -> com = (1,2)
	   Now: i = (0,1) -> com = (4,5) */
	//indico->com_no = i+3;
	indico->com_no = i;
	InitializeCriticalSection(&indico->write_cs);
	indico->write_buffer[0] = 0;
	create_write_buffer_event (indico);
	create_mode_change_event (indico);
	indico->read_buffer_len = 0;
	/* launch thread */
	_beginthread (indico_reader_thread, 0, indico);
    }
}

/* -------------------------------------------------------------------------*
    GUI thread logic
 * -------------------------------------------------------------------------*/

void
request_generator_combobox (HWND hwnd, int dlg_id, int indico_id, char* fmt, int times_ten)
{
    int val;
    char tmp[BUFLEN];

    SendDlgItemMessage (hwnd, dlg_id, WM_GETTEXT, BUFLEN, (LPARAM) tmp);
    if (1 != sscanf (tmp, "%d", &val)) {
	return;
    }
    if (times_ten) val *= 10;
    sprintf (tmp, fmt, val);
    make_write_buffer (&g_indico_thread[indico_id], "%s", tmp);
}

void
handle_edit (HWND hwnd, int edit_id, int feedback_id, int indico_id,
	     char* feedback_fmt, char* generator_rad_fmt, 
	     char* generator_fl_fmt, 
	     int rad_times_ten, int fl_times_ten)
{
    int gui_val;
    int feedback_val;
    char buf[BUFLEN];
    BOOL translated;
    char* gen_fmt;
    int times_ten;

    gui_val = GetDlgItemInt (hwnd, edit_id, (UINT*) &translated, 1);
    if (!translated) {
	return;
    }

    GetDlgItemText (hwnd, feedback_id, buf, BUFLEN);
    if (1 == sscanf (buf, feedback_fmt, &feedback_val)) {
	if (feedback_val == gui_val) return;
    }

    /* GCS FIX: Shouldn't snoop on other thread's memory like this */
    if (g_indico_thread[indico_id].mode == PM_PULSED_FLUORO) {
	gen_fmt = generator_fl_fmt;
	times_ten = fl_times_ten;
    } else {
	gen_fmt = generator_rad_fmt;
	times_ten = rad_times_ten;
    }

    if (times_ten) gui_val *= 10;
    sprintf (buf, gen_fmt, gui_val);
    make_write_buffer (&g_indico_thread[indico_id], "%s", buf);
}

int
find_scroll_target (int* values, int curr_value, int is_lineup)
{
    int *p, *q;
    p = q = values;

    if (*p > curr_value) return *p;
    while (*p) {
	if (*p > curr_value && is_lineup) {
		return *p;
	}
	if (*p >= curr_value && !is_lineup) {
		return *q;
	}
	q = p;
	p++;
    }
    return *q;
}

void
handle_kv_scroll (HWND hwnd, int dlg_id, int indico_id, int is_lineup)
{
    int kvp_values[] = { 40, 50, 60, 70, 80, 85, 90, 95, 100, 105, 110, 115, 120, 125, 0 };
    BOOL translated;
    int curr_value;

    curr_value = GetDlgItemInt (hwnd, dlg_id, (UINT*) &translated, 1);
    if (translated) {
	int new_value = find_scroll_target (kvp_values, curr_value, is_lineup);
	SetDlgItemInt (hwnd, dlg_id, new_value, 1);
    } else {
	/* GCS FIX: What to do here? */
    }
}

void
handle_ma_scroll (HWND hwnd, int dlg_id, int indico_id, int is_lineup)
{
    int rad_ma_values[] = { 40, 50, 63, 80, 100, 125, 0 };
    int pf_ma_values[] = { 25, 32, 40, 50, 63, 80, 0 };
    BOOL translated;
    int curr_value;
    int *ma_values;

    if (g_indico_thread[indico_id].mode == PM_PULSED_FLUORO) {
	ma_values = pf_ma_values;
    } else {
	ma_values = rad_ma_values;
    }

    curr_value = GetDlgItemInt (hwnd, dlg_id, (UINT*) &translated, 1);
    if (translated) {
	int new_value = find_scroll_target (ma_values, curr_value, is_lineup);
	SetDlgItemInt (hwnd, dlg_id, new_value, 1);
    } else {
	/* GCS FIX: What to do here? */
    }
}

void
handle_ms_scroll (HWND hwnd, int dlg_id, int indico_id, int is_lineup)
{
    int rad_ms_values[] = { 20, 32, 50, 63, 80, 100, 125, 160, 200, 250, 0 };
    int pf_ms_values[] = { 4, 6, 8, 10, 12, 15, 20, 25, 30, 35, 40, 45, 50, 0 };
    BOOL translated;
    int curr_value;
    int *ms_values;

    if (g_indico_thread[indico_id].mode == PM_PULSED_FLUORO) {
	ms_values = pf_ms_values;
    } else {
	ms_values = rad_ms_values;
    }

    curr_value = GetDlgItemInt (hwnd, dlg_id, (UINT*) &translated, 1);
    if (translated) {
	int new_value = find_scroll_target (ms_values, curr_value, is_lineup);
	SetDlgItemInt (hwnd, dlg_id, new_value, 1);
    } else {
	/* GCS FIX: What to do here? */
    }
}

void
format_status_message (int status_bar_no, char* string)
{
    int item = (status_bar_no == 0) ? IDC_MESSAGE_BAR1 : IDC_MESSAGE_BAR2;
    SetDlgItemText (g_hwin, item, string);
    free (string);
}

void
format_state_message (int status_bar_no, char* string)
{
    int item = (status_bar_no == 0) ? IDC_STATE_BAR_1 : IDC_STATE_BAR_2;
    SetDlgItemText (g_hwin, item, string);
    free (string);
}

void
format_kv_feedback (int status_bar_no, int value)
{
    char buf[BUFLEN];
    int item;
    
    /* Set item text */
    item = (status_bar_no == 0) ? IDC_KVP_1_FEEDBACK : IDC_KVP_2_FEEDBACK;
    _snprintf (buf, BUFLEN, "kvp %3d", value);
    SetDlgItemText (g_hwin, item, buf);
}

void
format_ma_feedback (int status_bar_no, int value)
{
    char buf[BUFLEN];
    int item;
    
    /* Set item text */
    item = (status_bar_no == 0) ? IDC_MA_1_FEEDBACK : IDC_MA_2_FEEDBACK;
    _snprintf (buf, BUFLEN, "mA %3d", value);
    SetDlgItemText (g_hwin, item, buf);
}

void
format_ms_feedback (int status_bar_no, int value)
{
    char buf[BUFLEN];
    int item;
    
    /* Set item text */
    item = (status_bar_no == 0) ? IDC_MS_1_FEEDBACK : IDC_MS_2_FEEDBACK;
    _snprintf (buf, BUFLEN, "ms %3d", value);
    SetDlgItemText (g_hwin, item, buf);
}

void
format_he_feedback (int status_bar_no, int value)
{
    char buf[BUFLEN];
    int item;
    
    /* Set item text */
    item = (status_bar_no == 0) ? IDC_HE_1_FEEDBACK : IDC_HE_2_FEEDBACK;
    _snprintf (buf, BUFLEN, "Anode: %3d %%", value);
    SetDlgItemText (g_hwin, item, buf);
}

void
format_hh_feedback (int status_bar_no, int value)
{
    char buf[BUFLEN];
    int item;
    
    /* Set item text */
    item = (status_bar_no == 0) ? IDC_HH_1_FEEDBACK : IDC_HH_2_FEEDBACK;
    _snprintf (buf, BUFLEN, "Housing: %3d %%", value);
    SetDlgItemText (g_hwin, item, buf);
}

void
format_mode_button (int indico_idx, int value)
{
    int item;

    item = (indico_idx == 0) ? IDC_RAD_FLUORO_BUTTON_1 : IDC_RAD_FLUORO_BUTTON_2;
    switch (value) {
    case PM_WARMUP:
	SetDlgItemText (g_hwin, item, "Warmup");
	break;
    case PM_RAD:
	SetDlgItemText (g_hwin, item, "Rad");
	break;
    case PM_PULSED_FLUORO:
	SetDlgItemText (g_hwin, item, "Fluoro");
	break;
    }
}

void
align_input (HWND hwnd, int gui_id, int feedback_id, char* fmt)
{
    int feedback_val;
    char buf[BUFLEN];

    GetDlgItemText (hwnd, feedback_id, buf, BUFLEN);
    if (1 == sscanf (buf, fmt, &feedback_val)) {
	sprintf (buf, "%d", feedback_val);
	SetDlgItemText (hwnd, gui_id, buf);
	//PostMessage (hwnd, WM_CTLCOLORSTATIC, (WPARAM) GetDC (GetDlgItem (hwnd, feedback_id)), (LPARAM) GetDlgItem (hwnd, feedback_id));
	RedrawWindow (GetDlgItem (hwnd, feedback_id), 0, 0, RDW_INVALIDATE);
    }
}

void
align_inputs (HWND hwnd)
{
    align_input (hwnd, IDC_KVP_1_EDIT, IDC_KVP_1_FEEDBACK, "kvp %d");
    align_input (hwnd, IDC_KVP_2_EDIT, IDC_KVP_2_FEEDBACK, "kvp %d");
    align_input (hwnd, IDC_MA_1_EDIT, IDC_MA_1_FEEDBACK, "mA %d");
    align_input (hwnd, IDC_MA_2_EDIT, IDC_MA_2_FEEDBACK, "mA %d");
    align_input (hwnd, IDC_MS_1_EDIT, IDC_MS_1_FEEDBACK, "ms %d");
    align_input (hwnd, IDC_MS_2_EDIT, IDC_MS_2_FEEDBACK, "ms %d");
}

LRESULT
ctlcolorstatic_brush (HWND hwnd, WPARAM wparam, int feedback_dlg_item, 
			int gui_dlg_item, char* fmt)
{
    int feedback_val, gui_val;
    char buf[BUFLEN];

    GetDlgItemText (hwnd, feedback_dlg_item, buf, BUFLEN);
    if (1 == sscanf (buf, fmt, &feedback_val)) {
	GetDlgItemText (hwnd, gui_dlg_item, buf, BUFLEN);
	if (1 == sscanf (buf, "%d", &gui_val)) {
	    if (gui_val == feedback_val) {
		return (LRESULT) NULL_BRUSH;
	    }
	}
    }
    SetBkMode ((HDC) wparam, TRANSPARENT);
    return (LRESULT) g_hbrush_red;
}

LRESULT
format_ctlcolorstatic (HWND hwnd, WPARAM wparam, LPARAM lparam)
{
    int feedback_dlg_item;

    feedback_dlg_item = GetDlgCtrlID ((HWND) lparam);

    switch (feedback_dlg_item) {
    case IDC_KVP_1_FEEDBACK:
	return ctlcolorstatic_brush (hwnd, wparam, feedback_dlg_item, IDC_KVP_1_EDIT, "kvp %d");
    case IDC_KVP_2_FEEDBACK:
	return ctlcolorstatic_brush (hwnd, wparam, feedback_dlg_item, IDC_KVP_2_EDIT, "kvp %d");
    case IDC_MA_1_FEEDBACK:
	return ctlcolorstatic_brush (hwnd, wparam, feedback_dlg_item, IDC_MA_1_EDIT, "mA %d");
    case IDC_MA_2_FEEDBACK:
	return ctlcolorstatic_brush (hwnd, wparam, feedback_dlg_item, IDC_MA_2_EDIT, "mA %d");
    case IDC_MS_1_FEEDBACK:
	return ctlcolorstatic_brush (hwnd, wparam, feedback_dlg_item, IDC_MS_1_EDIT, "ms %d");
    case IDC_MS_2_FEEDBACK:
	return ctlcolorstatic_brush (hwnd, wparam, feedback_dlg_item, IDC_MS_2_EDIT, "ms %d");
    }
    return FALSE;
}

void
shuffle_mode (int indico_idx)
{
    /* No need to lock 32-bit integer - it is atomic in win32. */
    switch (g_indico_thread[indico_idx].mode) {
    case PM_WARMUP:
	g_indico_thread[indico_idx].mode = PM_RAD;
	break;
    case PM_PULSED_FLUORO:
	g_indico_thread[indico_idx].mode = PM_WARMUP;
	break;
    case PM_RAD:
	g_indico_thread[indico_idx].mode = PM_PULSED_FLUORO;
	break;
    }
    SetEvent (g_indico_thread[indico_idx].mode_change_event);
}

LRESULT CALLBACK
dialog_callback (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int wmId, wmEvent;

    switch (message) {
    case WM_COMMAND:
	wmId    = LOWORD(wParam);
	wmEvent = HIWORD(wParam);
	if (wmEvent == EN_CHANGE) {
	    switch (wmId) {
		/* GCS FIX: This logic needs to be moved to the reader 
		    threads because generator FLM+ 
		    syntax may require multiple messages */
	    case IDC_KVP_1_EDIT:
		handle_edit (hwnd, wmId, IDC_KVP_1_FEEDBACK, 0, "kvp %d", "KV%03d", "FLK%03d", 0, 0);
		return TRUE;
	    case IDC_KVP_2_EDIT:
		handle_edit (hwnd, wmId, IDC_KVP_2_FEEDBACK, 1, "kvp %d", "KV%03d", "FLK%03d", 0, 0);
		return TRUE;
	    case IDC_MA_1_EDIT:
		handle_edit (hwnd, wmId, IDC_MA_1_FEEDBACK, 0, "mA %d", "MA%05d", "FLM%03d", 1, 1);
		return TRUE;
	    case IDC_MA_2_EDIT:
		handle_edit (hwnd, wmId, IDC_MA_2_FEEDBACK, 1, "mA %d", "MA%05d", "FLM%03d", 1, 1);
		return TRUE;
	    case IDC_MS_1_EDIT:
		handle_edit (hwnd, wmId, IDC_MS_1_FEEDBACK, 0, "ms %d", "MS%05d", "FLW%04d", 1, 0);
		return TRUE;
	    case IDC_MS_2_EDIT:
		handle_edit (hwnd, wmId, IDC_MS_2_FEEDBACK, 1, "ms %d", "MS%05d", "FLW%04d", 1, 0);
		return TRUE;
	    }
	}
	else if (wmEvent == BN_CLICKED) {
	    switch (wmId) {
	    case IDC_RAD_FLUORO_BUTTON_1:
		shuffle_mode (0);
		return TRUE;
	    case IDC_RAD_FLUORO_BUTTON_2:
		shuffle_mode (1);
		return TRUE;
	    }
	}
	return FALSE;
    case WM_INITDIALOG: {
	    HICON hIcon;

	    hIcon = LoadImage(g_hinstance,
		       MAKEINTRESOURCE(IDI_INDICO_SMALL),
		       IMAGE_ICON,
		       GetSystemMetrics(SM_CXSMICON),
		       GetSystemMetrics(SM_CYSMICON),
		       0);
	    if (hIcon) {
		SendMessage(hwnd, WM_SETICON, ICON_SMALL, (LPARAM)hIcon);
	    }
	    hIcon = LoadImage(g_hinstance,
		       MAKEINTRESOURCE(IDI_INDICO),
		       IMAGE_ICON,
		       GetSystemMetrics(SM_CXICON),
		       GetSystemMetrics(SM_CYICON),
		       0);
	    if (hIcon) {
		SendMessage(hwnd, WM_SETICON, ICON_BIG, (LPARAM)hIcon);
	    }
	}
	return TRUE;
    case WM_VSCROLL: {
	    HWND h_scrollbar = (HWND) lParam;
	    int nScrollCode = (int) LOWORD (wParam);
	    int scroll_id = GetDlgCtrlID (h_scrollbar);
	    int is_lineup = (nScrollCode == SB_LINEUP);

	    if (nScrollCode != SB_LINEUP && nScrollCode != SB_LINEDOWN) {
		return FALSE;
	    }

	    switch (scroll_id) {
	    case IDC_KV_1_SCROLL:
		handle_kv_scroll (hwnd, IDC_KVP_1_EDIT, 0, is_lineup);
		return TRUE;
	    case IDC_KV_2_SCROLL:
		handle_kv_scroll (hwnd, IDC_KVP_2_EDIT, 1, is_lineup);
		return TRUE;
	    case IDC_MA_1_SCROLL:
		handle_ma_scroll (hwnd, IDC_MA_1_EDIT, 0, is_lineup);
		return TRUE;
	    case IDC_MA_2_SCROLL:
		handle_ma_scroll (hwnd, IDC_MA_2_EDIT, 1, is_lineup);
		return TRUE;
	    case IDC_MS_1_SCROLL:
		handle_ms_scroll (hwnd, IDC_MS_1_EDIT, 0, is_lineup);
		return TRUE;
	    case IDC_MS_2_SCROLL:
		handle_ms_scroll (hwnd, IDC_MS_2_EDIT, 1, is_lineup);
		return TRUE;
	    }
	 }
    case UWM_UPDATE_MSGBAR:
	format_status_message (wParam, (char*) lParam);
	return TRUE;
    case UWM_UPDATE_KVP:
	format_kv_feedback (wParam, (int) lParam);
	return TRUE;
    case UWM_UPDATE_MA:
	format_ma_feedback (wParam, (int) lParam);
	return TRUE;
    case UWM_UPDATE_MS:
	format_ms_feedback (wParam, (int) lParam);
	return TRUE;
    case UWM_UPDATE_HE:
	format_he_feedback (wParam, (int) lParam);
	return TRUE;
    case UWM_UPDATE_HH:
	format_hh_feedback (wParam, (int) lParam);
	return TRUE;
    case UWM_ALIGN_INPUTS:
	align_inputs (hwnd);
	return TRUE;
    case UWM_UPDATE_STATEBAR:
	format_state_message (wParam, (char*) lParam);
	return TRUE;
    case UWM_UPDATE_MODE_BUTTON:
	format_mode_button (wParam, (int) lParam);
	return TRUE;
    case WM_CTLCOLORSTATIC:
	return format_ctlcolorstatic (hwnd, wParam, lParam);
    case WM_CLOSE:
    case WM_DESTROY:
	PostQuitMessage(0);
	break;
    default:
	return FALSE;
    }
    return FALSE;
}

BOOL
create_window (HINSTANCE hInstance, int nCmdShow)
{
    RECT client_size;
    DWORD dwStyle;
    LONG style;

    dwStyle = WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX 
		| WS_SIZEBOX | WS_MAXIMIZE;

    client_size.left = 0;
    client_size.top = 0;
    client_size.right = 100 - 1;
    client_size.bottom = 100 - 1;
    /* AdjustWindowRect (&client_size, dwStyle, TRUE); */

    g_hwin = CreateDialogIndirect (hInstance,
		    g_dialog_template,
		    (HWND) NULL,
		    dialog_callback);

#if defined (commentout)
    g_hwin = CreateWindowEx (
		WS_EX_APPWINDOW | WS_EX_OVERLAPPEDWINDOW,
		"Indico123",
		"INDICO",
		WS_DLGFRAME,
		CW_USEDEFAULT, 0,	// (x,y) position
		nWidth, nHeight,
		NULL,			// Parent window
		NULL,			// Menu
		hInstance,
		lpParam
		);
#endif

    style = GetWindowLong (g_hwin, GWL_STYLE);
    style = style | WS_SYSMENU;


#if defined (commentout)
    SetWindowPos (g_hwin, 
            HWND_TOPMOST,
            0, 
            0, 
            0, 
            0, 
            SWP_NOMOVE | SWP_NOSIZE | SWP_FRAMECHANGED);
#endif

    

    ShowWindow (g_hwin, nCmdShow);
    UpdateWindow (g_hwin);

    return TRUE;
}

void
register_dialog_class (HINSTANCE hInstance)
{
    WNDCLASSEX wcex;
    ATOM atom;
    HRSRC hrsrc;
    HGLOBAL hglb;

#if defined (commentout)
#endif
    wcex.cbSize         = sizeof(WNDCLASSEX);
    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = (WNDPROC) dialog_callback;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = DLGWINDOWEXTRA;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon (hInstance, (LPCTSTR)IDI_INDICO);
    wcex.hCursor        = LoadCursor (NULL, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH) (COLOR_WINDOW+1);
    //wcex.lpszMenuName   = (LPCSTR)IDC_ISE;
    //wcex.lpszMenuName   = NULL;
    wcex.lpszMenuName   = (LPCSTR)IDC_INDICO;
    wcex.lpszClassName  = "Indico123";
    wcex.hIconSm        = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_INDICO_SMALL);

    atom = RegisterClassEx(&wcex);

    hrsrc = FindResource(NULL, (LPCTSTR) IDD_INDICO_DUAL, RT_DIALOG);
    hglb = LoadResource(hInstance, hrsrc);
    g_dialog_template = LockResource(hglb);
}

void
load_dialog_bitmaps (void)
{
    g_hbitmap_preparing = LoadImage (
	g_hinstance,
	MAKEINTRESOURCE (IDB_BITMAP1),         // name or identifier of image
	IMAGE_BITMAP,                          // type of image
	0,                                     // desired width
	0,                                     // desired height
	LR_DEFAULTSIZE | LR_LOADMAP3DCOLORS    // load flags
    );
    g_hbitmap_prepared = LoadImage (
	g_hinstance,
	MAKEINTRESOURCE (IDB_BITMAP2),         // name or identifier of image
	IMAGE_BITMAP,                          // type of image
	0,                                     // desired width
	0,                                     // desired height
	LR_DEFAULTSIZE | LR_LOADMAP3DCOLORS    // load flags
    );
    g_hbitmap_exposing = LoadImage (
	g_hinstance,
	MAKEINTRESOURCE (IDB_BITMAP3),         // name or identifier of image
	IMAGE_BITMAP,                          // type of image
	0,                                     // desired width
	0,                                     // desired height
	LR_DEFAULTSIZE | LR_LOADMAP3DCOLORS    // load flags
    );
}

void
create_brushes (void)
{
    g_hbrush_red = CreateSolidBrush (RGB(255,100,100));
}

int APIENTRY
WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,
         LPSTR lpCmdLine, int nCmdShow)
{
    MSG msg;

    g_hinstance = hInstance;
    register_dialog_class (hInstance);

#if defined (commentout)
    LoadString (hInstance, IDC_ISE, szWindowClass, MAX_LOADSTRING);
    register_dialog_class (hInstance);
    globals.hAccelTable = LoadAccelerators (hInstance, (LPCTSTR)IDC_ISE);
#endif

    /* Set up shared memory */
    init_indico_shmem (&indico_info);

    /* It is not possible to make an empty accelerator table using the gui */
    g_haccel = LoadAccelerators (hInstance, (LPCTSTR)IDR_ACCELERATOR);

    load_dialog_bitmaps ();
    create_brushes ();

    create_window (hInstance, nCmdShow);
    create_indico_threads ();

    // Main message loop:
    while (GetMessage(&msg, NULL, 0, 0)) {
	if (!IsDialogMessage(g_hwin, &msg)) {
	    if (!TranslateAccelerator (msg.hwnd, g_haccel, &msg)) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	    }
	}
    }

    return ((int) msg.wParam);
}
