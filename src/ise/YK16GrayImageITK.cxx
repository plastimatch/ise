#include "YK16GrayImage.h"
#include "YK16GrayImageITK.h"
#include "itkImageRegionIterator.h"

void
CopyYKImage2ItkImage (
    YK16GrayImage* pYKImage, UnsignedShortImageType::Pointer& spTarImage)
{
    if (pYKImage == NULL)
        return;
    UnsignedShortImageType::RegionType region = spTarImage->GetRequestedRegion();
    UnsignedShortImageType::SizeType tmpSize = region.GetSize();

    int sizeX = tmpSize[0];
    int sizeY = tmpSize[1];

    if (sizeX < 1 || sizeY <1)
        return;

    itk::ImageRegionIterator<UnsignedShortImageType> it(spTarImage, region);

    int i = 0;
    for (it.GoToBegin() ; !it.IsAtEnd(); ++it)
    {
        it.Set(pYKImage->m_pData[i]);
        i++;
    }
}

void
CopyItkImage2YKImage (
    UnsignedShortImageType::Pointer& spSrcImage, YK16GrayImage* pYKImage)
{
	if (pYKImage == NULL)
		return;
	UnsignedShortImageType::RegionType region = spSrcImage->GetRequestedRegion();
	UnsignedShortImageType::SizeType tmpSize = region.GetSize();

	int sizeX = tmpSize[0];
	int sizeY = tmpSize[1];

	if (sizeX < 1 || sizeY <1)
		return;

	itk::ImageRegionIterator<UnsignedShortImageType> it(spSrcImage, region);

	int i = 0;
	for (it.GoToBegin() ; !it.IsAtEnd() ; ++it)
	{
		pYKImage->m_pData[i] = it.Get();
		i++;
	}
}
