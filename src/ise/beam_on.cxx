/* -----------------------------------------------------------------------
   See COPYRIGHT.TXT and LICENSE.TXT for copyright and license information
   ----------------------------------------------------------------------- */
#ifdef _WIN32
#include <windows.h>
#include <io.h>
#include <process.h>
#include <direct.h>
#endif
#include "beam_on.h"
#include <stdlib.h>
#include "ise.h"
#include "ise_framework.h"
#include "ise_globals.h"
#include "filewrite.h"
#include "debug.h"
#include "rotating.h"

#include "ise_rsc.h"
#include "ise_gdi.h"
#include "ise_gl.h"
#include "ise_config.h"

#include <commctrl.h>

#define BUFLEN 1024

//#define BEAMON_FROM_NITOOL

//#include "stdafx.h"

/* ---------------------------------------------------------------------------- *
    Global variables
 * ---------------------------------------------------------------------------- */
HANDLE*     beam_on_thread_handle;
int32       error_bo = 0;
TaskHandle  taskHandle_bo;
char        errBuff_bo[2048];

const char  chanOut[] = "myDAQ1/port2/line0";

uInt32      w_data1[1], w_data2[1];
int32       bm_written;

double completation_percentage;

double extra_cycle_number = 1.2;

extern int complete_info_need_written, cfg_reload_finished;
extern int rec_time_written;

/* ---------------------------------------------------------------------------- *
    Function declarations
 * ---------------------------------------------------------------------------- */
static void beam_on_thread (void* v);
static int beam_on_error (void);
static void usleep (__int64 usec);
static void set_beam_on (void);
static void set_beam_off (void);
static void highlight_dialog_buttons (void);
static void handle_button_stop (void);
static void highlight_dialog_buttons_win (HWND hwnd);
static void set_button_state (HWND hwnd, int dlg_item_id, BOOL pushed);
static void highlight_source_menu (HWND hwnd);
static int hwnd_to_idx (HWND hwnd);
static void update_lut (HWND hwnd, long bot, long top);

/* ---------------------------------------------------------------------------- *
    Functions
 * ---------------------------------------------------------------------------- */
void 
beam_on_init (void)
{
    globals.beam_on_flag = 0;
    
    w_data1[0] = 1;
    w_data2[0] = 0;

    taskHandle_bo = 0;
    completation_percentage = 0;

#ifndef BEAMON_FROM_NITOOL
    //digital output to control beam gate
    DAQmxErrChk (DAQmxClearTask (taskHandle_bo));
    DAQmxErrChk (DAQmxCreateTask ("", &taskHandle_bo));
    DAQmxErrChk (DAQmxCreateDOChan(taskHandle_bo,chanOut,"",DAQmx_Val_ChanForAllLines));
    DAQmxErrChk (DAQmxStartTask (taskHandle_bo));

    //reset control signal to 0
    DAQmxErrChk (DAQmxWriteDigitalU32(taskHandle_bo,1,1,10.0,DAQmx_Val_GroupByChannel,w_data2,&bm_written,NULL));
#endif

    beam_on_thread_handle = (HANDLE*) malloc(sizeof(HANDLE));

    beam_on_thread_handle[0] = (HANDLE*) _beginthread (beam_on_thread, 0, NULL);

    if (!beam_on_thread_handle) 
    {
        MessageBox(NULL, "Beam on thread error!", "Error!!", MB_OK);
        return;
    }

Error:

    if (error_bo)
    {
        beam_on_error ();
    }

    return;
}


static void
beam_on_thread (void* v)
{
    double period = (((double)1.0/globals.mw_rate_hz)+0.002);
    double time_1 = 0, time_2 = 0;

    LARGE_INTEGER clock_count;
    LARGE_INTEGER clock_freq;

    QueryPerformanceFrequency (&clock_freq);
    
    while(1)
    {
        //no table rotation mode
        while (!globals.rotate_platform_flag)
        {
            while (globals.program_state == PROGRAM_STATE_STOPPED && !globals.rotate_platform_flag) {
                set_beam_off ();
                Sleep (20);
            }

            while (globals.program_state == PROGRAM_STATE_RECORDING && !globals.rotate_platform_flag) {
                while (!cfg_reload_finished) Sleep (20);
                set_beam_on ();
                //take data for certain time
                QueryPerformanceCounter(&clock_count);
                time_1 = (double) clock_count.QuadPart / (double) clock_freq.QuadPart;
                time_2 = time_1;
                while (time_2 - time_1 <= (double) (globals.mw_cycle_number + extra_cycle_number) / globals.mw_rate_hz && !globals.rotate_platform_flag && globals.program_state == PROGRAM_STATE_RECORDING)
                {
                    Sleep (20);
                    QueryPerformanceCounter(&clock_count);
                    time_2 = (double) clock_count.QuadPart / (double) clock_freq.QuadPart;
                    globals.degree_now = (time_2 - time_1) / ((double) (globals.mw_cycle_number + extra_cycle_number) / globals.mw_rate_hz) * globals.rotate_degree_total;
                    if (globals.degree_now > globals.rotate_degree_total)
                        globals.degree_now = globals.rotate_degree_total;
                }
                completation_percentage = (double) (time_2 - time_1) / ((globals.mw_cycle_number + extra_cycle_number) / globals.mw_rate_hz) * 100;
                if (completation_percentage > 100) completation_percentage = 100.0;
                //turn off beam and make recording to stop
                set_beam_off ();
                complete_info_need_written = 1;
                handle_button_stop ();
                highlight_dialog_buttons ();
                //while (!rec_time_written) Sleep (20);
                if (completation_percentage >= 100) {
                    MessageBox(NULL, "Acquisition completed!", "Congratulations!", MB_OK);
                }
                else {
                    MessageBox(NULL, "Acquisition not completed!", "Attention!!", MB_OK);
                    globals.degree_now = 0;
                }
            }

            Sleep (20);

        }
        //"step and shoot" mode and computer can control beam gate, when platform stops
        while (globals.rotate_platform_flag && !globals.rotate_ctns_flag)
        {
            while (globals.program_state == PROGRAM_STATE_STOPPED && globals.rotate_platform_flag && !globals.rotate_ctns_flag) {
                set_beam_off ();
                Sleep (20);
            }

            while (globals.program_state == PROGRAM_STATE_RECORDING && globals.rotate_stop_flag && globals.rotate_platform_flag && !globals.rotate_ctns_flag){
                set_beam_on ();
                Sleep (20);
            }
            
            while (globals.program_state == PROGRAM_STATE_RECORDING && !globals.rotate_stop_flag && globals.rotate_platform_flag && !globals.rotate_ctns_flag){
                set_beam_off ();
                Sleep (20);
            }

            Sleep (20);
            
        }
        //continuous mode
        while (globals.rotate_platform_flag && globals.rotate_ctns_flag)
        {
            while (globals.program_state == PROGRAM_STATE_STOPPED && globals.rotate_platform_flag && globals.rotate_ctns_flag) {
                set_beam_off ();
                Sleep (20);
            }

            while (globals.program_state == PROGRAM_STATE_RECORDING && globals.rotate_platform_flag && globals.rotate_ctns_flag) {
                set_beam_on ();
                Sleep (20);
            }

            Sleep (20);

        }

        Sleep (20);

    }

}

static void set_beam_on (void)
{
    DAQmxErrChk (DAQmxWriteDigitalU32(taskHandle_bo,1,1,10.0,DAQmx_Val_GroupByChannel,w_data1,&bm_written,NULL));

    globals.beam_on_flag = 1;
    globals.beam_on_write_flag = 1;

    Error:

    if (error_bo)
    {
        beam_on_error ();
    }
}

static void set_beam_off (void)
{
    DAQmxErrChk (DAQmxWriteDigitalU32(taskHandle_bo,1,1,10.0,DAQmx_Val_GroupByChannel,w_data2,&bm_written,NULL));

    globals.beam_on_flag = 0;
    globals.beam_on_write_flag = 0;

    Error:

    if (error_bo)
    {
        beam_on_error ();
    }
}

int
beam_on_stop (HANDLE* beam_on_thread_handle)
{
    /* Wait for threads to finish */
    debug_printf ("BeamOn waiting for threads\n");

    WaitForMultipleObjects (1,beam_on_thread_handle,TRUE,300);
	
    debug_printf ("BeamOn threads done!\n");
    CloseHandle (beam_on_thread_handle);
    
    return 0;
}

static int
beam_on_error (void)
{
    if (DAQmxFailed (error_bo))
      DAQmxGetExtendedErrorInfo (errBuff_bo, 2048);

    if (taskHandle_bo != 0)
    {
      DAQmxStopTask (taskHandle_bo);
      DAQmxClearTask (taskHandle_bo);
    }

    if (error_bo)
    {
      debug_printf ("DAQmx Error: %d\n", error_bo);
      MessageBox(NULL, "Beam on task error!", "Error!!", MB_OK);
    }

    return 0;
}

static void
usleep (__int64 usec)
{
    HANDLE timer;
    LARGE_INTEGER ft;
    
    ft.QuadPart = -(10*usec);

    timer = CreateWaitableTimer (NULL, TRUE, NULL);
    if (NULL == timer)
    {
        debug_printf ("Create waitable timer failed.\n");
        return;
    }
    if (!SetWaitableTimer (timer, &ft, 0, NULL, NULL, 0))
    {
        debug_printf ("Set waitable timer failed.\n");
        return;
    }
    
    if (WaitForSingleObject (timer, INFINITE) != WAIT_OBJECT_0)
    {
        debug_printf ("Wait for single object failed.\n");
        return;
    }
    CloseHandle (timer);
    return;
}

static void
handle_button_stop ()
{
    if (globals.program_state == PROGRAM_STATE_GRABBING 
	|| globals.program_state == PROGRAM_STATE_RECORDING)
    {
	ise_fluoro_stop_grabbing ();
    }
    globals.ig.write_flag = 0;
    globals.program_state = PROGRAM_STATE_STOPPED;
}

static void
highlight_dialog_buttons (void)
{
    int idx;
    for (idx = 0; idx < globals.num_panels; idx++) {
	highlight_dialog_buttons_win (globals.win[idx].hwin);
    }
}

static void
highlight_dialog_buttons_win (HWND hwnd)
{
    switch (globals.program_state) {
    case PROGRAM_STATE_STOPPED:
	set_button_state (hwnd, IDC_BUTTON_STOP, TRUE);
	set_button_state (hwnd, IDC_BUTTON_GRAB, FALSE);
	set_button_state (hwnd, IDC_BUTTON_REC, FALSE);
	set_button_state (hwnd, IDC_BUTTON_REPLAY, FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REWBEG), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_PAUSE), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REPLAY), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_FF), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_FRAME), TRUE);
	break;
    case PROGRAM_STATE_GRABBING:
	set_button_state (hwnd, IDC_BUTTON_STOP, FALSE);
	set_button_state (hwnd, IDC_BUTTON_GRAB, TRUE);
	set_button_state (hwnd, IDC_BUTTON_REC, FALSE);
	set_button_state (hwnd, IDC_BUTTON_REPLAY, FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REWBEG), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_PAUSE), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REPLAY), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_FF), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_FRAME), FALSE);
	break;
    case PROGRAM_STATE_RECORDING:
	set_button_state (hwnd, IDC_BUTTON_STOP, FALSE);
	set_button_state (hwnd, IDC_BUTTON_GRAB, TRUE);
	set_button_state (hwnd, IDC_BUTTON_REC, TRUE);
	set_button_state (hwnd, IDC_BUTTON_REPLAY, FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REWBEG), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_PAUSE), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REPLAY), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_FF), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_FRAME), FALSE);
	break;
    case PROGRAM_STATE_REPLAYING:
	set_button_state (hwnd, IDC_BUTTON_STOP, FALSE);
	set_button_state (hwnd, IDC_BUTTON_GRAB, FALSE);
	set_button_state (hwnd, IDC_BUTTON_REC, FALSE);
	set_button_state (hwnd, IDC_BUTTON_REPLAY, TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REWBEG), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_PAUSE), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REPLAY), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_FF), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_FRAME), TRUE);
	break;
    }

    highlight_source_menu (hwnd);

    if (globals.ig.write_dark_flag) {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_WRITE_DARK, MF_BYCOMMAND | MF_CHECKED);
    } else {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_WRITE_DARK, MF_BYCOMMAND | MF_UNCHECKED);
    }

    if (globals.auto_window_level) {
	set_button_state (hwnd, IDC_BUTTON_AWL, TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_TOP), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_BOT), FALSE);
    } else {
	set_button_state (hwnd, IDC_BUTTON_AWL, FALSE);
	gdi_update_lut_slider (hwnd_to_idx(hwnd), 0, MAXGREY-1);
	update_lut (hwnd, 0, MAXGREY-1);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_TOP), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_BOT), TRUE);
    }

    if (globals.hold_bright_frame) {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_HOLD_BRIGHT, MF_BYCOMMAND | MF_CHECKED);
    } else {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_HOLD_BRIGHT, MF_BYCOMMAND | MF_UNCHECKED);
    }

    if (globals.drop_dark_frames) {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_DROP_DARK, MF_BYCOMMAND | MF_CHECKED);
    } else {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_DROP_DARK, MF_BYCOMMAND | MF_UNCHECKED);
    }

    if (globals.rotate_platform_flag) {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_ROTATE_PLATFORM, MF_BYCOMMAND | MF_CHECKED);
    } else {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_ROTATE_PLATFORM, MF_BYCOMMAND | MF_UNCHECKED);
    }

    if (globals.tracking_flag) {
	set_button_state (hwnd, IDC_BUTTON_TRACK, TRUE);
    } else {
	set_button_state (hwnd, IDC_BUTTON_TRACK, FALSE);
    }

    if (globals.gating_flag) {
	set_button_state (hwnd, IDC_BUTTON_GATE, TRUE);
    } else {
	set_button_state (hwnd, IDC_BUTTON_GATE, FALSE);
    }
    UpdateWindow (hwnd);
}

static void
set_button_state (HWND hwnd, int dlg_item_id, BOOL pushed)
{
    //SendDlgItemMessage (hwnd, dlg_item_id, BM_SETSTATE, pushed, 0);
    SendDlgItemMessage (hwnd, dlg_item_id, BM_SETCHECK, 
			pushed ? BST_CHECKED : BST_UNCHECKED, 0);
    UpdateWindow (GetDlgItem(hwnd, dlg_item_id));
}

static void
highlight_source_menu (HWND hwnd)
{
    switch (globals.program_state) 
    {
        case PROGRAM_STATE_STOPPED:
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_ENABLED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_ENABLED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_ENABLED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_ENABLED);
            EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_ENABLED);
	    break;
        case PROGRAM_STATE_GRABBING:
        case PROGRAM_STATE_RECORDING:
        case PROGRAM_STATE_REPLAYING:
        default:
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_GRAYED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_GRAYED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_GRAYED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_GRAYED);
            EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_GRAYED);
	    break;
    }

    switch (globals.ig.image_source) 
    {
        case ISE_IMAGE_SOURCE_MATROX_LORES_FLUORO:
        case ISE_IMAGE_SOURCE_MATROX_HIRES_FLUORO:
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_CHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_UNCHECKED); 
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_UNCHECKED);
            CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_UNCHECKED);
	    break;
        case ISE_IMAGE_SOURCE_BITFLOW_LORES_FLUORO:
        case ISE_IMAGE_SOURCE_BITFLOW_HIRES_FLUORO:
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_CHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_UNCHECKED);
            CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_UNCHECKED);
	    break;
        case ISE_IMAGE_SOURCE_SIMULATED_LORES_FLUORO:
        case ISE_IMAGE_SOURCE_SIMULATED_HIRES_FLUORO:
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_UNCHECKED); 
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_CHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_UNCHECKED);
            CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_UNCHECKED);
	    break;
        case ISE_IMAGE_SOURCE_FILE_LORES_FLUORO:
        case ISE_IMAGE_SOURCE_FILE_HIRES_FLUORO:
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_UNCHECKED); 
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_UNCHECKED);
            CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_CHECKED);
	    break;
        case ISE_IMAGE_SOURCE_INTERNAL_FLUORO:
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_UNCHECKED); 
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_CHECKED);
            CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_UNCHECKED);
	    break;
        default:
	    break;
    }
}

static int
hwnd_to_idx (HWND hwnd)
{
    if (hwnd == globals.win[0].hwin) {
	return 0;
    } else {
	return 1;
    }
}

static void
update_lut (HWND hwnd, long bot, long top)
{
    int idx;

    idx = hwnd_to_idx (hwnd);
    gl_update_lut (idx, (unsigned short) bot, (unsigned short) top);
}
