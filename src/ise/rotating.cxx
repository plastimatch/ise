/* -----------------------------------------------------------------------
   See COPYRIGHT.TXT and LICENSE.TXT for copyright and license information
   ----------------------------------------------------------------------- */
#ifdef _WIN32
#include <windows.h>
#include <io.h>
#include <process.h>
#include <direct.h>
#endif
#include "rotating.h"
#include <stdlib.h>
#include "ise.h"
#include "ise_framework.h"
#include "ise_globals.h"
#include "filewrite.h"
#include "debug.h"

#include "ise_rsc.h"
#include "ise_gdi.h"
#include "ise_gl.h"
#include "ise_config.h"

#include <commctrl.h>

#define BUFLEN 1024

//#include "stdafx.h"

/* ---------------------------------------------------------------------------- *
    Global variables
 * ---------------------------------------------------------------------------- */
HANDLE* rotate_thread_handle;
//static int rotate_degree_total, rotate_time_sec, degreestep, rotate_stop_time_ms;
//static char rotate_command [BUFLEN];
int need_reset_motor, rotation_complete, warning_showed, degree, complete_info_need_written, cfg_reload_finished;
char* degree_rotate;
double steps_per_degree = 100000/624.8;

extern double extra_cycle_number;
extern int rec_time_written;

/* ---------------------------------------------------------------------------- *
    Function declarations
 * ---------------------------------------------------------------------------- */
static void rotate_thread (void* v);
static void highlight_dialog_buttons (void);
static void handle_button_stop (void);
static void highlight_dialog_buttons_win (HWND hwnd);
static void set_button_state (HWND hwnd, int dlg_item_id, BOOL pushed);
static void highlight_source_menu (HWND hwnd);
static int hwnd_to_idx (HWND hwnd);
static void update_lut (HWND hwnd, long bot, long top);
static void load_rotate_cfg (void);
static char* trim_left (char* s);
static void trim_right (char* s);
static void interpret_line (char* key, char* val);
static void rotate_command_init (void);
static void rotation_complete_warning (void);
static void rotation_not_complete_warning (void);
static void stop_rotation (void);
static void rotate_reinit (void);

/* ---------------------------------------------------------------------------- *
    Functions
 * ---------------------------------------------------------------------------- */
void 
rotate_init (void)
{
    long IsDriverLoaded;

    load_rotate_cfg ();
    rotate_command_init ();

    need_reset_motor = 1; 
    rotation_complete = 0;
    warning_showed = 0;
    degree = 0;
    degree_rotate = 0;
    globals.rotate_stop_flag = 0;
    complete_info_need_written = 0;
	
    rotate_thread_handle = (HANDLE*) malloc(sizeof(HANDLE));
    if (!rotate_thread_handle) 
    {
        MessageBox(NULL, "Rotatory platform thread error!", "Error!!", MB_OK);
        return;
    }    	

    LPCSTR DriverPath="C:\\rotating platform\\test\\VxmDriver.dll";

    IsDriverLoaded=LoadDriver(DriverPath);

    //rotatory platform initialization
    if (!IsDriverLoaded)
    {
        MessageBox(NULL, "Rotatory platform driver error!", "Error!!", MB_OK);
        return;
    }
    else
    {
        PortOpen(3,9600);
        if (!PortIsOpen())
        {
            MessageBox(NULL, "Rotatory platform port error!", "Error!!", MB_OK);
            return;
        }
        else
        {
            //set angle to 0
            PortSendCommands("K");
            PortWaitForChar("^",0);
            PortSendCommands("F,C,IA1M-0,R");
            PortWaitForChar("^",0);
    		
            rotate_thread_handle[0] = (HANDLE*) _beginthread (rotate_thread, 0, NULL);
        }
    }

    return;
}

static void
rotate_thread (void* v)
{
    double time_1 = 0, time_2 = 0;

    LARGE_INTEGER clock_count;
    LARGE_INTEGER clock_freq;

    QueryPerformanceFrequency (&clock_freq);

    while(1)
    {
        while (globals.rotate_platform_flag)//if rotate option checked
        {	
            //when stopped before rotation completed
	    while (!rotation_complete && !warning_showed && !need_reset_motor && globals.program_state == PROGRAM_STATE_STOPPED && globals.rotate_platform_flag) 
	    {
                stop_rotation ();
                rotate_reinit ();
                //while (!rec_time_written) Sleep (20);
                rotation_not_complete_warning ();
	    }
            //when rotation completed
            while (rotation_complete && !warning_showed && !need_reset_motor && globals.program_state == PROGRAM_STATE_RECORDING && globals.rotate_platform_flag) 
	    {    	    
                stop_rotation ();
                rotation_complete_warning ();
                rotate_reinit ();
                //while (!rec_time_written) Sleep (20);
                MessageBox(NULL, "Rotation completed and ready to go!", "Congratulations!", MB_OK);
	    }
            //rotation
	    while (!rotation_complete && need_reset_motor && globals.program_state == PROGRAM_STATE_RECORDING && globals.rotate_platform_flag) 
	    {
                warning_showed = 0;
                need_reset_motor = 0;
                load_rotate_cfg ();
                rotate_command_init ();
                PortSendCommands("F,C,IA1M-0,R");
                PortWaitForChar("^",0);

                //continuous
                while (globals.rotate_ctns_flag && !rotation_complete && globals.rotate_platform_flag && globals.program_state == PROGRAM_STATE_RECORDING && globals.rotate_platform_flag)
                {
                    PortSendCommands(globals.rotate_command);
                    while (!rotation_complete && globals.rotate_platform_flag && globals.program_state == PROGRAM_STATE_RECORDING)
                    {
                        degree_rotate = MotorPosition(1);
                        degree = atoi(degree_rotate);
                        globals.degree_now = (double) degree / steps_per_degree;
                        if (degree >= (globals.rotate_degree_total - 0.5) * steps_per_degree) rotation_complete = 1;
                        //debug_printf ("degree_now %.3f\n", globals.degree_now);
                        //update_queue_status (0);
                        Sleep (200);
                    }
                }

                //step and shoot
                while (!globals.rotate_ctns_flag && !rotation_complete && globals.rotate_platform_flag && globals.program_state == PROGRAM_STATE_RECORDING)
                {
                        PortSendCommands(globals.rotate_command);
                        PortWaitForChar("^",0);
                        //Sleep (1000);
                        globals.rotate_stop_flag = 1;
                        while (!globals.beam_on_write_flag) Sleep(20);
                        //wait until enough data collected
                        QueryPerformanceCounter(&clock_count);
                        time_1 = (double) clock_count.QuadPart / (double) clock_freq.QuadPart;
                        time_2 = time_1;
                        while (time_2 - time_1 <= (double) (globals.mw_cycle_number + extra_cycle_number) / globals.mw_rate_hz && globals.rotate_platform_flag && globals.program_state == PROGRAM_STATE_RECORDING)
                        {
                            Sleep (20);
                            QueryPerformanceCounter(&clock_count);
                            time_2 = (double) clock_count.QuadPart / (double) clock_freq.QuadPart;
                        }
                        globals.rotate_stop_flag = 0;
                        while (globals.beam_on_write_flag) Sleep(20);
                        degree_rotate = MotorPosition(1);
                        degree = atoi(degree_rotate);
                        globals.degree_now = (double) degree / steps_per_degree;
                        if (degree >= (globals.rotate_degree_total - 0.5) * steps_per_degree) {
                            rotation_complete = 1;
                        }
                        Sleep (20);
                }

	    }
            
        }
        //when rotate option unchecked before complete
        while (!rotation_complete && !warning_showed && !need_reset_motor && !globals.rotate_platform_flag)
        {
            stop_rotation ();
            rotate_reinit ();
            rotation_not_complete_warning ();
        }
        //when rotate option unchecked before recording
        while (!globals.rotate_platform_flag && globals.program_state == PROGRAM_STATE_RECORDING)
        {
            load_rotate_cfg ();
            rotate_command_init ();
            cfg_reload_finished = 1;
        }
        while (!globals.rotate_platform_flag && globals.program_state == PROGRAM_STATE_STOPPED)
        {
            cfg_reload_finished = 0;
        }
    }
}

static void rotation_complete_warning (void)
{
    handle_button_stop ();
    highlight_dialog_buttons ();
    warning_showed = 1;
    //MessageBox(NULL, "Rotation completed!", "Congratulations!", MB_OK);
    globals.degree_now = 0;
}

static void rotation_not_complete_warning (void)
{
    warning_showed = 1;
    MessageBox(NULL, "Rotation not completed but ready to go!", "Attention!!", MB_OK);
    globals.degree_now = 0;
}

static void stop_rotation (void)
{
    globals.rotate_stop_flag = 0;
    PortSendCommands("K");
    PortWaitForChar("^",0);
    rotation_complete = 0;
    need_reset_motor = 1;
    complete_info_need_written = 1;
}

static void rotate_reinit (void)
{
    int degree_return_int = 0;
    int degree_return_or = 1;
    double steps_360_degree = 57618.438;

    char return_command [1024];

    load_rotate_cfg ();
    rotate_command_init ();
    
    globals.rotate_stop_flag = 0;
    rotation_complete = 0;

    degree_rotate = MotorPosition(1);
    degree = atoi(degree_rotate);

    degree_return_or = (int) ((degree + steps_360_degree / 2) / steps_360_degree) - (int) degree / steps_360_degree;
    if (degree_return_or) {
        degree_return_int = (int) (((int) ((degree + steps_360_degree / 2) / steps_360_degree)) * steps_360_degree - degree + 0.5);
    }
    else {
        degree_return_int = (int) (- ((int) ((degree + steps_360_degree / 2) / steps_360_degree)) * steps_360_degree + degree + 0.5);
    }
    if (degree_return_or)
        sprintf (return_command, "F,C,S1M6000,I1M%d,R", degree_return_int);
    else
        sprintf (return_command, "F,C,S1M6000,I1M-%d,R", degree_return_int);

    PortSendCommands(return_command);
    PortWaitForChar("^",0);

    PortSendCommands("F,C,IA1M-0,R");
    PortWaitForChar("^",0);
    need_reset_motor = 1;
}

int
rotate_stop (HANDLE* rotate_thread_handle)
{
    /* Wait for threads to finish */
    debug_printf ("ROTATE waiting for threads\n");

    PortSendCommands("K");
    PortWaitForChar("^",0);
    PortClose();
    WaitForMultipleObjects (1,rotate_thread_handle,TRUE,300);
	
    debug_printf ("ROTATE threads done!\n");
    CloseHandle (rotate_thread_handle);
    return 0;
}


static void
handle_button_stop ()
{
    if (globals.program_state == PROGRAM_STATE_GRABBING 
	|| globals.program_state == PROGRAM_STATE_RECORDING)
    {
	ise_fluoro_stop_grabbing ();
    }
    globals.ig.write_flag = 0;
    globals.program_state = PROGRAM_STATE_STOPPED;
}

static void
highlight_dialog_buttons (void)
{
    int idx;
    for (idx = 0; idx < globals.num_panels; idx++) {
	highlight_dialog_buttons_win (globals.win[idx].hwin);
    }
}

static void
highlight_dialog_buttons_win (HWND hwnd)
{
    switch (globals.program_state) {
    case PROGRAM_STATE_STOPPED:
	set_button_state (hwnd, IDC_BUTTON_STOP, TRUE);
	set_button_state (hwnd, IDC_BUTTON_GRAB, FALSE);
	set_button_state (hwnd, IDC_BUTTON_REC, FALSE);
	set_button_state (hwnd, IDC_BUTTON_REPLAY, FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REWBEG), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_PAUSE), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REPLAY), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_FF), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_FRAME), TRUE);
	break;
    case PROGRAM_STATE_GRABBING:
	set_button_state (hwnd, IDC_BUTTON_STOP, FALSE);
	set_button_state (hwnd, IDC_BUTTON_GRAB, TRUE);
	set_button_state (hwnd, IDC_BUTTON_REC, FALSE);
	set_button_state (hwnd, IDC_BUTTON_REPLAY, FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REWBEG), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_PAUSE), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REPLAY), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_FF), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_FRAME), FALSE);
	break;
    case PROGRAM_STATE_RECORDING:
	set_button_state (hwnd, IDC_BUTTON_STOP, FALSE);
	set_button_state (hwnd, IDC_BUTTON_GRAB, TRUE);
	set_button_state (hwnd, IDC_BUTTON_REC, TRUE);
	set_button_state (hwnd, IDC_BUTTON_REPLAY, FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REWBEG), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_PAUSE), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REPLAY), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_FF), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_FRAME), FALSE);
	break;
    case PROGRAM_STATE_REPLAYING:
	set_button_state (hwnd, IDC_BUTTON_STOP, FALSE);
	set_button_state (hwnd, IDC_BUTTON_GRAB, FALSE);
	set_button_state (hwnd, IDC_BUTTON_REC, FALSE);
	set_button_state (hwnd, IDC_BUTTON_REPLAY, TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REWBEG), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_PAUSE), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_REPLAY), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_BUTTON_FF), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_FRAME), TRUE);
	break;
    }

    highlight_source_menu (hwnd);

    if (globals.ig.write_dark_flag) {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_WRITE_DARK, MF_BYCOMMAND | MF_CHECKED);
    } else {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_WRITE_DARK, MF_BYCOMMAND | MF_UNCHECKED);
    }

    if (globals.auto_window_level) {
	set_button_state (hwnd, IDC_BUTTON_AWL, TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_TOP), FALSE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_BOT), FALSE);
    } else {
	set_button_state (hwnd, IDC_BUTTON_AWL, FALSE);
	gdi_update_lut_slider (hwnd_to_idx(hwnd), 0, MAXGREY-1);
	update_lut (hwnd, 0, MAXGREY-1);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_TOP), TRUE);
	EnableWindow (GetDlgItem(hwnd,IDC_SLIDER_BOT), TRUE);
    }

    if (globals.hold_bright_frame) {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_HOLD_BRIGHT, MF_BYCOMMAND | MF_CHECKED);
    } else {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_HOLD_BRIGHT, MF_BYCOMMAND | MF_UNCHECKED);
    }

    if (globals.drop_dark_frames) {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_DROP_DARK, MF_BYCOMMAND | MF_CHECKED);
    } else {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_DROP_DARK, MF_BYCOMMAND | MF_UNCHECKED);
    }

    if (globals.rotate_platform_flag) {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_ROTATE_PLATFORM, MF_BYCOMMAND | MF_CHECKED);
    } else {
	CheckMenuItem (GetMenu(hwnd), IDM_OPTIONS_ROTATE_PLATFORM, MF_BYCOMMAND | MF_UNCHECKED);
    }

    if (globals.tracking_flag) {
	set_button_state (hwnd, IDC_BUTTON_TRACK, TRUE);
    } else {
	set_button_state (hwnd, IDC_BUTTON_TRACK, FALSE);
    }

    if (globals.gating_flag) {
	set_button_state (hwnd, IDC_BUTTON_GATE, TRUE);
    } else {
	set_button_state (hwnd, IDC_BUTTON_GATE, FALSE);
    }
    UpdateWindow (hwnd);
}

static void
set_button_state (HWND hwnd, int dlg_item_id, BOOL pushed)
{
    //SendDlgItemMessage (hwnd, dlg_item_id, BM_SETSTATE, pushed, 0);
    SendDlgItemMessage (hwnd, dlg_item_id, BM_SETCHECK, 
			pushed ? BST_CHECKED : BST_UNCHECKED, 0);
    UpdateWindow (GetDlgItem(hwnd, dlg_item_id));
}

static void
highlight_source_menu (HWND hwnd)
{
    switch (globals.program_state) 
    {
        case PROGRAM_STATE_STOPPED:
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_ENABLED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_ENABLED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_ENABLED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_ENABLED);
            EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_ENABLED);
	    break;
        case PROGRAM_STATE_GRABBING:
        case PROGRAM_STATE_RECORDING:
        case PROGRAM_STATE_REPLAYING:
        default:
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_GRAYED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_GRAYED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_GRAYED);
	    EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_GRAYED);
            EnableMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_GRAYED);
	    break;
    }

    switch (globals.ig.image_source) 
    {
        case ISE_IMAGE_SOURCE_MATROX_LORES_FLUORO:
        case ISE_IMAGE_SOURCE_MATROX_HIRES_FLUORO:
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_CHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_UNCHECKED); 
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_UNCHECKED);
            CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_UNCHECKED);
	    break;
        case ISE_IMAGE_SOURCE_BITFLOW_LORES_FLUORO:
        case ISE_IMAGE_SOURCE_BITFLOW_HIRES_FLUORO:
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_CHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_UNCHECKED);
            CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_UNCHECKED);
	    break;
        case ISE_IMAGE_SOURCE_SIMULATED_LORES_FLUORO:
        case ISE_IMAGE_SOURCE_SIMULATED_HIRES_FLUORO:
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_UNCHECKED); 
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_CHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_UNCHECKED);
            CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_UNCHECKED);
	    break;
        case ISE_IMAGE_SOURCE_FILE_LORES_FLUORO:
        case ISE_IMAGE_SOURCE_FILE_HIRES_FLUORO:
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_UNCHECKED); 
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_UNCHECKED);
            CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_CHECKED);
	    break;
        case ISE_IMAGE_SOURCE_INTERNAL_FLUORO:
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_MATROX, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_BITFLOW, MF_BYCOMMAND | MF_UNCHECKED); 
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_SYNTHETIC, MF_BYCOMMAND | MF_UNCHECKED);
	    CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_INTERNAL, MF_BYCOMMAND | MF_CHECKED);
            CheckMenuItem (GetMenu(hwnd), IDM_SOURCE_FILE, MF_BYCOMMAND | MF_UNCHECKED);
	    break;
        default:
	    break;
    }
}

static int
hwnd_to_idx (HWND hwnd)
{
    if (hwnd == globals.win[0].hwin) {
	return 0;
    } else {
	return 1;
    }
}

static void
update_lut (HWND hwnd, long bot, long top)
{
    int idx;

    idx = hwnd_to_idx (hwnd);
    gl_update_lut (idx, (unsigned short) bot, (unsigned short) top);
}

static void
load_rotate_cfg (void)
{
    FILE* fp;
    char buf[BUFLEN];
    char *b, *s;

    fp = fopen ("rotate.cfg", "r");
    if (!fp) return;

    while (fgets (buf, BUFLEN, fp)) {
	b = trim_left(buf);
	if (*b == '#') continue;
	s = strstr (b, "=");
	if (!s) continue;
	*s++ = 0;
	s = trim_left (s);
	trim_right (b);
	trim_right (s);

	interpret_line (b, s);
    }

    fclose (fp);    

    globals.rotate_stop_flag = 0;
    globals.rotate_degree_total = globals.rotate_degree_step + globals.rotate_degree_total;
    globals.rotate_time_sec = (double) globals.rotate_degree_total / globals.rotate_degree_step * globals.mw_cycle_number / globals.mw_rate_hz;
}

/* Return pointer to first non-whitespace character */
static char*
trim_left (char* s)
{
    while (isspace(*s)) s++;
    return s;
}

/* Truncate whitespace at end of string */
static void
trim_right (char* s)
{
    int j;
    for (j = strlen(s)-1; j >= 0; j--) {
	if (isspace(s[j])) {
	    s[j] = 0;
	}
    }
}

static void
interpret_line (char* key, char* val)
{
    if (!strcmp (key, "rotate_degree_total")) {
	globals.rotate_degree_total = atoi (val);
    }
    if (!strcmp (key, "rotate_ctns_flag")) {
	globals.rotate_ctns_flag = atoi (val);
    }
    if (!strcmp (key, "rotate_step_degree")) {
	globals.rotate_degree_step = atoi (val);
    }
    if (!strcmp (key, "rotate_stop_time_ms")) {
	globals.rotate_stop_time_ms = atoi (val);
    }
    if (!strcmp (key, "mw_rate_mhz")) {
	globals.mw_rate_hz = atoi (val);
        globals.mw_rate_hz = (double) globals.mw_rate_hz / 1000;
    }
    if (!strcmp (key, "mw_cycle_number")) {
	globals.mw_cycle_number = atoi (val);
    }
    if (!strcmp (key, "can_control_beam_gate")) {
	globals.can_control_beam_gate = atoi (val);
    }
}
static void
rotate_command_init (void)
{
    int speed, degree, degree_step;

    degree = globals.rotate_degree_total * steps_per_degree;
    speed = (int) (degree / globals.rotate_time_sec + 0.5);
    degree_step = globals.rotate_degree_step * steps_per_degree;

    if (degree > 16777215)
    {
        MessageBox(NULL, "Total rotation angle too big! \n\nChange configuration in rotate.cfg file and restart this program.", "Error!!", MB_OK);
        return;
    }
    if (speed <= 1)
    {
        MessageBox(NULL, "Rotation speed too small! \n\nChange configuration in rotate.cfg file and restart this program.", "Error!!", MB_OK);
        return;
    }

    if (globals.rotate_ctns_flag)
    {
        sprintf (globals.rotate_command, "F,C,S1M%d,I1M%d,R", speed, degree);
    }
    else
    {
        sprintf (globals.rotate_command, "F,C,S1M6000,I1M%d,R", degree_step);
    }
}