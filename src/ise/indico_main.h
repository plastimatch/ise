/* -------------------------------------------------------------------------*
    See COPYRIGHT for copyright information.
 * -------------------------------------------------------------------------*/
#ifndef __indico_main_h__
#define __indico_main_h__

#define UWM_UPDATE_MSGBAR      (WM_APP + 1)
#define UWM_UPDATE_KVP         (WM_APP + 2)
#define UWM_UPDATE_MA          (WM_APP + 3)
#define UWM_UPDATE_MS          (WM_APP + 4)
#define UWM_UPDATE_HE          (WM_APP + 5)
#define UWM_UPDATE_HH          (WM_APP + 6)
#define UWM_ALIGN_INPUTS       (WM_APP + 7)
#define UWM_UPDATE_STATEBAR    (WM_APP + 8)
#define UWM_UPDATE_MODE_BUTTON (WM_APP + 9)

#endif
