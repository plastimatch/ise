/* -------------------------------------------------------------------------*
    See COPYRIGHT for copyright information.
 * -------------------------------------------------------------------------*/
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include "isewatch.h"
#include "isewatch_client.h"

int
main (int argc, char* argv[])
{
    int i;
    map_shared_memory ();
    for (i = 0; i < 100; i++) {
	signal_watchdog ();
	Sleep (1000/8);
    }
    return 0;
}
